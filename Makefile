TARGET_EXEC ?= grid.js

BUILD_DIR ?= ./build
SRC_DIRS ?= ./src

MAIN := ./src/main.cxx
TEST := ./src/Math.cxx
SRCS := $(shell find $(SRC_DIRS) -name *.cpp -or -name *.c -or -name *.s)
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)
LIBS =  -lm 
INC_DIRS := $(shell find $(SRC_DIRS) -type d)
#CPPFLAGS += -lstdc++ #-O2

ifeq ($(CXX), g++)
LIBS += -lSDL2 -lSDL2_ttf -lSDL2_image -lSDL2_gfx
INC_DIRS += 
CPPFLAGS += -g3
LDFLAGS += -g3
else
CPPFLAGS += -D__EMSCRIPTEN__ -O2 -s DISABLE_EXCEPTION_CATCHING=0 
LIBS += -s USE_SDL=2 -s USE_SDL_IMAGE=2 -s USE_SDL_GFX=2 -s USE_SDL_TTF=2 -s SDL2_IMAGE_FORMATS='["jpg", "bmp", "png", "xpm"]'
LIBS += -s DISABLE_EXCEPTION_CATCHING=0
EMFLAGS += -s MODULARIZE=1 -s EXPORT_NAME="GridModule" -s FORCE_FILESYSTEM=1 -s EXPORTED_RUNTIME_METHODS='["ccall", "addFunction", "allocateUTF8"]'
EMFLAGS += -s RESERVED_FUNCTION_POINTERS=20 -s TOTAL_MEMORY=1gb --preload-file Assets -g4 -s DISABLE_EXCEPTION_CATCHING=0
EMFLAGS += -s ALLOW_MEMORY_GROWTH=1 -s ASSERTIONS=1 -s NO_EXIT_RUNTIME=1
CCPFLAGS += $(LIBS)
#CPPFLAGS += -s USE_SDL=2 -s USE_WEBGL2=1 -s USE_SDL_TTF=2 -s USE_SDL_IMAGE=2 -s SDL2_IMAGE_FORMATS='["jpg","bmp","png","xpm"]' -s USE_SDL_GFX=2
#LDFLAGS +=  -s USE_LIBJPEG=1 -s USE_SDL=2 -s USE_WEBGL2=1 -s ALLOW_MEMORY_GROWTH=1 -s USE_SDL_TTF=2 -s USE_SDL_GFX=2 -s WASM=1
#EMFLAGS += -s MODULARIZE=1 -s EXPORT_NAME="GridModule" -s FORCE_FILESYSTEM=1 -s EXTRA_EXPORTED_RUNTIME_METHODS='["ccall", "addFunction", "allocateUTF8"]' -s WASM=1 -s ASSERTIONS=1
#EMFLAGS += -s RESERVED_FUNCTION_POINTERS=20 -s USE_SDL=2 -s USE_WEBGL2=1 -s USE_SDL_TTF=2 -s USE_SDL_IMAGE=2 -s SDL2_IMAGE_FORMATS='["jpg","bmp","png","xpm"]' -s USE_SDL_GFX=2
#EMFLAGS += --preload-file Assets -O2 -s TOTAL_MEMORY=1073741824 -s DISABLE_DEPRECATED_FIND_EVENT_TARGET_BEHAVIOR=1 -s DISABLE_EXCEPTION_CATCHING=2
endif

#INC_DIRS += /usr/include
INC_DIRS += ./include/
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

#LFLAGS += -lm -Wall
CPPFLAGS += $(INC_FLAGS) -MMD -MP
CFLAGS = $(LFLAGS) -MMD -MP

build: $(BUILD_DIR)/$(TARGET_EXEC)
	cp -r Assets $(BUILD_DIR)

$(BUILD_DIR)/$(TARGET_EXEC): $(BUILD_DIR)/main.o $(OBJS)
	$(CXX) $(BUILD_DIR)/main.o $(OBJS) -o $@ $(LDFLAGS) $(LIBS) $(EMFLAGS)

# c source
$(BUILD_DIR)/%.c.o: %.c
	$(MKDIR_P) $(dir $@)
	$(CC) $(CFLAGS) -g -c $< -o $@

# c++ source
$(BUILD_DIR)/%.cpp.o: %.cpp
	$(MKDIR_P) $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

$(BUILD_DIR)/main.o: $(MAIN)
	$(MKDIR_P) $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(LIBS) $(EMFLAGS) -c $< -o $@

.PHONY: clean

clean:
	$(RM) -fr $(BUILD_DIR)/*

-include $(DEPS)

MKDIR_P ?= mkdir -p

all: build
.PHONY: all

test: $(OBJS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $(TEST) -o $(BUILD_DIR)/$(TEST).o
	$(CXX) $(BUILD_DIR)/$(TEST).o  $(OBJS) -o $(BUILD_DIR)/UT $(LIBS) $(LDFLAGS)

.PHONY: test
