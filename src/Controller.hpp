#ifndef CONTROLLER_H
#define CONTROLLER_H
#include <SDL2/SDL.h>
#include "Grid.hpp"
#include "Renderer/Renderer.hpp"
#include "Renderer/types.hpp"
#include "Players.hpp"
#include "Token.hpp"
#include <vector>
#include <map>
#include <string>
#include <memory>

extern std::shared_ptr<std::map<int, RenderObject*>> RoMap;

enum SystemState {
SYSTEM_STATE_DRAWING,
SYSTEM_STATE_SELECTION
};

struct LineConfig {
	Uint32 Color = 0xFF000000; // A8 R8 G8 B8
	Uint16 Width = 3;
};


class Controller {
	public:
		bool ActiveDrag;
		SystemState sstate;
		MouseState mstate;
		SDL_Point MousePos;
		Renderer* Re;
		RenderObject* RoInClipoard;
		LineConfig lineConfig;
		std::map<std::string, int> LayerNameToID;
		std::map<int, Layer*> Layers;
		std::map<int, RenderObject*> romap;
		Player activePlayer;
		std::vector<Player> players;
		std::vector<Token> tokens;
		bool spectator;
		int roid;
		GridConfig _GridConfig;
		Controller(
			Renderer* Re,
			GridConfig gc);
		~Controller();
		int GetNewRoID();
		void HandleEvents();
		void HandleEvent(SDL_Event e);
		void Update();
		void NewLine();
		void EndLine();
		void SelectRO();
		void HandleMouse();
		void ChangeState(SystemState state);
		int AddRo(int TextureID);
		int AddRoIn(int TextureID, int x, int y);
		int CopySelectedRo();
		int RemoveSelectedRo();
		int SwitchLayer(int LayerID);
		int SwitchLayer(std::string name);
		Controller();
		void CreateEmptySystem();
		int NewImage(std::string filename);
		int InsertImage(std::string filename, int id);
		int SetRO(int id, int index, int layer, int idTexture );
		int ResizeRO(int roid, SDL_Point p);
		int PosRO(int roid, SDL_Point p);
		void VisibilityMode();
		int SerializedUpdate(std::string serialized);
		void ChangeToLayer(int roID, int layerID, bool update = true);
		void PrintLayerState();
		void ChangeLineConfig(Uint32 color, Uint16 width);
		void GenDemoGM();
		void AddNewUser(int id, char* name, unsigned int flags = PLAYER_FLAG_PLAYER);
		int SwitchUser(int id);
		int ModifyUser(int id, char* name, unsigned int flags = PLAYER_FLAG_NO_FLAG);
		int MakeToken(int ro_id, int user_id = 0);
		void RemoveRo(int ro_id);
		void RemoveSelected();
		void CopySelected();
		void PasteFromClipBoard();
	private:
		SDL_Point DragOffset;
		RenderLine* activeLine;
		void SelectionEvent(SDL_Event e);
		void DrawingEvent(SDL_Event e);
		void MouseRelease();
		void HandleKeyboardSelectionEvent(SDL_Event e);
		void PrintState();
		SDL_Cursor* cursor;
		void HandleTextureDrag();
		void HandleLineDrag();
		/* Handles Release a Texture */
		void MouseReleasedTexture();
		/* Handles Released a Line */
		void MouseReleasedLine();
		int SerializedUpdateRenderTexture(std::string serialized);
		int SerializedUpdateRenderLine(std::string serialized);
		int SerializedUpdatePlayer(std::string serialized);
		int SerializedUpdateToken(std::string serialized);
		int SerializedUpdateLayer(std::string serialized);
		int CopyRT(RenderTexture re, bool update = false);
		int CopyRL(RenderLine rl);
		void SendRightClickEvent();
	        bool TextureInTokens(RenderObject* ro);
};

#endif
