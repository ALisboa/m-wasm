#include <iostream>
#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#include "ExternalLibrary.hpp"
#endif
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include "Grid.hpp"
#include "Renderer/Renderer.hpp"
#include "Renderer/TextureManager.hpp"
#include <string>
#include <ctime>
#include <memory>
#include <map>
#include "Controller.hpp"
#include "MathHelper.hpp"
#include "MessageHelper.hpp"
#include <chrono>
#include "Serializer.hpp"
#include "Players.hpp"
#define FRAME 17

Renderer re;
int done = false;
Controller controller;
TextureManager* TM;
std::shared_ptr<std::map<int, RenderObject*>> RoMap;

std::chrono::high_resolution_clock::time_point last_frame = std::chrono::high_resolution_clock::now();
std::chrono::high_resolution_clock::time_point current_frame;
std::chrono::duration<double, std::milli> diff;
SDL_BlendMode BLENDMODE_AMINUS = SDL_BLENDMODE_INVALID;
RestartParser RP = NULL;

void UpdateGame(){
  controller.Update();
}


void Draw(){
  re.Render();
}

void QUIT(){
  //delete &re;
  SDL_Quit();
}

void GameLoop(){
  current_frame = std::chrono::high_resolution_clock::now();
  diff = current_frame - last_frame; ///
  if(diff.count() > 0.017){
    UpdateGame();
    //Draw();
    last_frame = current_frame;
  }
  if(done){
    QUIT();
  }
}


int main(int argc, char ** argv ){
  if(SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0){
    std::cout << "Unable to Initialize SDL2: " << SDL_GetError() << std::endl;
    return EXIT_FAILURE;
  }
  if(IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG | IMG_INIT_WEBP) == 0){
    std::cout << "Error Initializing SDL2_IMG: " << IMG_GetError() <<std::endl;
    return EXIT_FAILURE;
  }
  SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1");
  SDL_SetHint("SDL_EMSCRIPTEN_KEYBOARD_ELEMENT", "#canvas");
  BLENDMODE_AMINUS = SDL_ComposeCustomBlendMode(
    SDL_BLENDFACTOR_SRC_COLOR, SDL_BLENDFACTOR_DST_COLOR, SDL_BLENDOPERATION_ADD,
    SDL_BLENDFACTOR_SRC_ALPHA, SDL_BLENDFACTOR_DST_ALPHA, SDL_BLENDOPERATION_REV_SUBTRACT);
  TM = new TextureManager();
  re.TM = TM;
  re.InitRenderer();
  controller.Re = &re;
  controller._GridConfig.Anchors = CORNER;
  controller._GridConfig.Size = {16, 16};
  controller._GridConfig.TileSize = {64, 64};
  controller._GridConfig.BackgroundColor = 0xFFFFFFFF;
  controller._GridConfig.width = 2;
  controller._GridConfig.GridColor = 0x2D2D2D55;
  re.GC = &controller._GridConfig;
  re.Grid = dynamic_cast<RenderTexture*>(re.CreateGrid(controller._GridConfig));
  //initCircle();
  re.ResizeWindow({
    controller._GridConfig.Size.x * controller._GridConfig.TileSize.x, 
    controller._GridConfig.Size.y * controller._GridConfig.TileSize.y
  });
  controller.CreateEmptySystem();
#ifdef __EMSCRIPTEN__
  emscripten_set_main_loop(GameLoop, 0, false);
#else
  controller.ChangeState(SYSTEM_STATE_DRAWING);
  controller.GenDemoGM();
  controller.SwitchUser(1);
  while(done != true){
    GameLoop();
  }
#endif
  return EXIT_SUCCESS;
}
