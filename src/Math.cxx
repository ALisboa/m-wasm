#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch/catch.hpp"
#include <SDL2/SDL.h>
#include "MathHelper.hpp"
#include <cereal/types/string.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/archives/xml.hpp>
#include "Renderer/RenderObject.hpp"
#include <iostream>
#include <sstream>
#include <vector>
#include "Serializer.hpp"
#include "MessageHelper.hpp"

RestartParser RP;
TextureManager* TM;
std::shared_ptr<std::map<int, RenderObject*>> RoMap;

SDL_BlendMode BLENDMODE_AMINUS = SDL_BLENDMODE_INVALID;

TEST_CASE("ROTATE POINT ON 0,0", "[math]"){
    auto point =  rotate({0, -1}, 90);
    SECTION( "ROTATE 0,-1 90 degrees") {
        REQUIRE ( (point.x == 1 && point.y == 0));
    }
    SECTION("ROTATE 1,0 90 degrees"){
        point = rotate({1, 0}, 90);
        REQUIRE ( (point.x == 0 && point.y == 1 ));
    }
    SECTION("ROTATE 0, 1 90°"){
        point = rotate({0, 1}, 90);
        REQUIRE ( point.x == -1);
        REQUIRE ( point.y == 0);
    }
    SECTION("ROTATE -1,0 90°"){
        point = rotate({-1, 0}, 90);
        REQUIRE ( point.x == 0);
        REQUIRE ( point.y == -1);
    }
}

TEST_CASE("MANHATTAN DISTANCE", "[math]"){
    SDL_Point a, b;
    a = {0, 0};
    b = {5, 5};
    REQUIRE ( ManhattanDistance(a, b) == 10);
}

TEST_CASE("SerialTest_1", "[Serialization]"){
	std::ostringstream ss;
	class RenderLine a;
	a.id = 1;
	a.Type = RENDER_TYPE_LINE;
	a.Start = {0, 0};
	a.End = { 1024, 1024 };
	a.Z = 3;
	Texture t;
	t.id = 0;
	t.filename = "hellow";
	RenderTexture rt;
	rt.id = 2;
	rt.Type = RENDER_TYPE_TEXTURE;
	rt.Z = 2;
	rt.Position = {252, 252};
	rt.Size = {252, 256};
	rt.TextureID = t.id;
	{
		cereal::XMLOutputArchive archive(ss);
		archive(a);
	}
	SerializedObj so;
	so.type = SERIAL_TYPE_RENDER_LINE;
	so.archive = ss.str();
	so.archive.erase(std::remove(so.archive.begin(), so.archive.end(), '\n'), so.archive.end() );
	std::stringstream s2;
	{
		cereal::XMLOutputArchive archive(s2);
		archive(so);
	}
	std::string st2 = s2.str();
	st2.erase(std::remove(st2.begin(), st2.end(), '\n'), st2.end() );
	std::stringstream s3(st2);

	SerializedObj so1 = so;
	so = {};
	{
		cereal::XMLInputArchive iarchive(s3);
		iarchive(so);
	}

	RenderLine l;
	{
		std::stringstream auxss(so.archive);
		cereal::XMLInputArchive ia(auxss);
		ia(l);
	}
	REQUIRE(l.id == a.id);
	REQUIRE( so1.type == so.type );

	SerializedObj so2;
	so2.type = SERIAL_TYPE_RENDER_TEXTURE;
	{
		std::stringstream oss;
		{
			cereal::XMLOutputArchive archive(oss);
			archive(rt);
		}
		st2 = oss.str();
		st2.erase(std::remove(st2.begin(), st2.end(), '\n'), st2.end() );
		so2.archive = st2;
	}
	{
		std::stringstream oss;
		{
			cereal::XMLOutputArchive archive(oss);
			archive(so2);
		}
		st2 = oss.str();
		st2.erase(std::remove(st2.begin(), st2.end(), '\n'), st2.end() );
	}
	SerializedObj sf;
	{
		std::stringstream ssaux(st2);
		cereal::XMLInputArchive archive(ssaux);
		archive(sf);
	}
	RenderTexture rt2;
	{
		std::stringstream ssaux(sf.archive);
		cereal::XMLInputArchive archive(ssaux);
		archive(rt2);
	}

	REQUIRE( rt.id == rt2.id );

	
}

/*
TEST_CASE("POLYGON MAKING", "[math]"){
	Point p = {400, 400};
	std::vector<Ray> segments;
	segments.push_back({{-1, -1},{1024, -1}});
	segments.push_back({{1024, -1},{1024, 1024}});
	segments.push_back({{1024, 1024},{-1, 1024}});
	segments.push_back({{-1, 1024},{-1, -1}});

	segments.push_back({{50, 50}, {500, 300}});

	Ray a, b;
	a = {
		p,
		{p.x + 25, p.y + 25}
	};

	b = segments[2];
	auto s = Intersect(a, b);

	auto poly = GenSightPoly(p, segments);

}

*/
TEST_CASE("MATH", "[math]"){
	auto res = Distance(0,0 , 2,0, 1, 1);
	REQUIRE( res == 1 );
	res = Distance(0,0, 1,0, 1, 1);
	REQUIRE( res == 1 );
	res = Distance(0,0, 1,0, -1, -1);
	REQUIRE( res == 1 );
	res = Distance(0, 10, 10, 0, 10, 10);
	REQUIRE( res < 10 );
	res = Distance(0, 10, 10, 0, 0, 0);
	REQUIRE( res < 10 );
}

TEST_CASE("Serial Test Playerw", "string"){
	Player* origin = new Player();
	origin->flags = PLAYER_FLAG_DM;
	origin->id = 5;
	origin->name = "GM";
	std::string serializedOjb = Serialize(origin);
	std::cout << "Serialized Object"
			  << std::endl << serializedOjb << std::endl;
	Player back = Deserialize<Player>(serializedOjb);
	REQUIRE(origin->flags == back.flags);
	REQUIRE(origin->id == back.id);
	REQUIRE(origin->name.compare(back.name) == 0);
}

TEST_CASE("Serial Test Layer", "string"){
	Layer* l = new Layer();
	l->id = 2;
	l->alpha = 100;
	l->status = LAYER_STATUS_VISIBLE;
	std::string obj_archive = Serialize(l);
	std::cout << "Serialized Object"
			  << std::endl << obj_archive << std::endl;
	Layer lb = Deserialize<Layer>(obj_archive);
	REQUIRE(l->id == lb.id);
	REQUIRE(l->alpha == lb.alpha);
	REQUIRE(l->status == lb.status);
}

TEST_CASE("Serial Visibility Layer", "string"){
	VisibilityLayer* l = new VisibilityLayer();
	l->id = 5;
	l->ros.push_back(2);
	l->visionPoints.push_back({2, 5});
	l->alpha = 255;
	l->status = LAYER_STATUS_ACTIVE | LAYER_STATUS_INITIALLIZED;
	std::string obj_archive = Serialize(l);
	std::cout << "Serialized Object" << std::endl
			  << obj_archive << std::endl;
	Layer lb = Deserialize<Layer>(obj_archive);
	REQUIRE(lb.layer_type == l->layer_type);
	VisibilityLayer lc = Deserialize<VisibilityLayer>(obj_archive);
	REQUIRE(lc.visionPoints[0] == l->visionPoints[0]);
}

TEST_CASE("SERIAL TOKEN", "string"){
	Token* tkn = new Token();
	tkn->id = 5;
	tkn->user_id = 6;
	tkn->ro_id = 12;
	std::string obj_archive = Serialize(tkn);
	std::cout << "Serialized Object" << std::endl
			  << obj_archive << std::endl;
	Token tknb = Deserialize<Token>(obj_archive);
	REQUIRE(tkn->id == tknb.id);
	REQUIRE(tkn->ro_id == tknb.ro_id);
	REQUIRE(tkn->user_id == tknb.user_id);
}

TEST_CASE("JSON RIGHT CLICK", "strign"){
	RightClickStatus s;
	RenderTexture rt;
	rt.id = 2;
	rt.Z = 2;
	rt.layer = 3;
	s.roSelected = 1;
	s.ro = &rt;
	s.systemState = 1;
	s.layerId = 2;
	std::string ss = MakeJson(s);
	std::cout << ss << std::endl;
	REQUIRE(ss.empty() != true);
}
