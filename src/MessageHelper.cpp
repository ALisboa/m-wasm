#include "MessageHelper.hpp"
#include <iostream>
#define PARSER_WSMG "1"
#define PARSER_LOG "2"
#define PARSER_ERROR "3"
#define PARSER_CUSTOM "4"
#define PARSER_RIGHT_CLICK "5"

void SendUpdate(std::string message){
	_PrintMessage(PARSER_WSMG, message);
}
void SendMessage(std::string message){
	_PrintMessage(PARSER_LOG, message);
}
void SendError(std::string message){
	_PrintMessage(PARSER_ERROR, message);
}

void SendRightClick(std::string message){
	_PrintMessage(PARSER_RIGHT_CLICK, message);
}

void _PrintMessage(std::string premessage, std::string message){
	if(RP != NULL){
		RP();
	}
	std::cout << premessage << std::endl;
	std::cout << message << std::endl;
}
