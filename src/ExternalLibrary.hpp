#ifndef __EXTERNALLIBRARY_H_
#define __EXTERNALLIBRARY_H_
#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#include "Controller.hpp"
#include "Renderer/Renderer.hpp"
#include "MessageHelper.hpp"
#ifndef EMSCRIPTEN_KEEPALIVE
#define EMSCRIPTEN_KEEPALIVE
#endif

extern Controller controller;
extern Renderer re;
extern RestartParser RP;
extern int done;

void QUIT();


extern "C" {
    void EMSCRIPTEN_KEEPALIVE ChangeLineConfig(int color, int width);
    void EMSCRIPTEN_KEEPALIVE ResizeWindow(int w, int h);
    void EMSCRIPTEN_KEEPALIVE SetGridWidth(int size);
    void EMSCRIPTEN_KEEPALIVE ToggeVisionMode();
    void EMSCRIPTEN_KEEPALIVE SetTileSize(int x, int y);
    void EMSCRIPTEN_KEEPALIVE SetGridSize(int x, int y);
    void EMSCRIPTEN_KEEPALIVE SetGridColor(Uint32 color);
    void EMSCRIPTEN_KEEPALIVE SetGridBackgroundColor(Uint32 color);
    int EMSCRIPTEN_KEEPALIVE AddImage(const char* filename);
    int EMSCRIPTEN_KEEPALIVE InsertImage(const char* filename, int id);
    int EMSCRIPTEN_KEEPALIVE AddRo(int TextureID);
    int EMSCRIPTEN_KEEPALIVE AddRoIn(int TextureID, int x, int y);
    int EMSCRIPTEN_KEEPALIVE SetRO(int id, int index, int layer, int idTexture);
    int EMSCRIPTEN_KEEPALIVE ResizeRO(int roid, int sizex, int sizey);
    int EMSCRIPTEN_KEEPALIVE PosRO(int roid, int posx, int posy);
    const char * EMSCRIPTEN_KEEPALIVE GetError();
    void EMSCRIPTEN_KEEPALIVE SetRestartParser(void * funcPointer);
    void EMSCRIPTEN_KEEPALIVE Q();
    void EMSCRIPTEN_KEEPALIVE SetState(int input );
    int EMSCRIPTEN_KEEPALIVE SwitchLayerByName(std::string name);
    int EMSCRIPTEN_KEEPALIVE SwitchLayerByID(int id);
    int EMSCRIPTEN_KEEPALIVE ToggleSpectator();
    int EMSCRIPTEN_KEEPALIVE SerializedUpdate(char * string);
    void EMSCRIPTEN_KEEPALIVE print(char * string);
    void EMSCRIPTEN_KEEPALIVE GetSelectedObject();
    void EMSCRIPTEN_KEEPALIVE PrintCurrentLayers();
    void EMSCRIPTEN_KEEPALIVE GenDM();
    int EMSCRIPTEN_KEEPALIVE SwitchUser(int id);
    int EMSCRIPTEN_KEEPALIVE ModifyUser(int id, char * string);
    int EMSCRIPTEN_KEEPALIVE MakeToken(int ro_id);
    void EMSCRIPTEN_KEEPALIVE RemoveRo(int id);
    void EMSCRIPTEN_KEEPALIVE RemoveSelected();
    void EMSCRIPTEN_KEEPALIVE CopySelected();
    void EMSCRIPTEN_KEEPALIVE PasteFromClipboard();
    void EMSCRIPTEN_KEEPALIVE ChangeRoLayer(int ro_id, int layer_id);
}

#endif
#endif // __EXTERNALLIBRARY_H_
