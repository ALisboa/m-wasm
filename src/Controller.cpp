#include "Controller.hpp"
#include "MathHelper.hpp"
#include <iostream>
#include <sstream>
#include <cmath>
#include <SDL2/SDL_image.h>
#include "MessageHelper.hpp"
#include "Serializer.hpp"
#include <algorithm>

Controller::Controller(Renderer* Re,   GridConfig gc) {
    this->Re = Re;
    this->_GridConfig = gc;
    RoMap = std::make_shared<std::map<int, RenderObject*>>(this->romap);
    roid = 1;
}

Controller::Controller(){
    sstate = SYSTEM_STATE_SELECTION;
    roid = 1;
    spectator = false;
    RoMap = std::make_shared<std::map<int, RenderObject*>>(romap);
}

Controller::~Controller(){
  
}

void Controller::NewLine(){
  RenderLine* line = new RenderLine();
  line->color = lineConfig.Color;
  line->width = lineConfig.Width;
  activeLine = line;
  activeLine->Type = RENDER_TYPE_LINE;
  activeLine->Z = 1;
  activeLine->id = -1;
  activeLine->Start = AproxToAnchor(MousePos, _GridConfig);
  activeLine->End = {activeLine->Start.x+1, activeLine->Start.y+1};
  (*RoMap)[-1] = line;
  Re->AddRoToActiveLayer(line);
}

void Controller::EndLine() {
  if (activeLine == nullptr) {
    return;
  }
  activeLine->End = AproxToAnchor(activeLine->End, _GridConfig);
  Layers[activeLine->layer]->RemoveRo(-1);
  activeLine->id = GetNewRoID();
  (*RoMap)[activeLine->id] = (*RoMap)[-1];
  (*RoMap).erase(-1);
  Layers[activeLine->layer]->ros.push_back(activeLine->id);
  UpdateObject(activeLine);
  activeLine = NULL;
}

void Controller::SelectRO() {
    bool selected = false;
    for(int i = Re->activeLayer->ros.size(); i-- > 0;)
    {
        RenderObject* ro = (*RoMap)[Re->activeLayer->ros[i]];
        if( ro->removed != true && ro->Inside(MousePos.x, MousePos.y)) {
          if(activePlayer.flags != PLAYER_FLAG_DM) {
            if(!TextureInTokens(ro)){
              break;
            }
          }
          if(ro->Type == RENDER_TYPE_TEXTURE){
            DragOffset = MousePos - dynamic_cast<RenderTexture*>(ro)->Position;
            ActiveDrag = true;
          }
          else{
            DragOffset = MousePos;
          }
          Re->ClearSelectedObject();
          Re->SetSelectedObject(ro);
          selected = true;
          break;
        }
    }
    if(!selected){
        Re->ClearSelectedObject();
    }
}

void Controller::HandleTextureDrag(){
    RenderTexture* r = (RenderTexture*)Re->SelectedObject;
    switch(mstate){
    case MOUSE_STATE_ARROW:
      if(r){
        r->Position = { 
          (int)(MousePos.x - DragOffset.x),
          (int)(MousePos.y - DragOffset.y)
        };
      }
      break;
    case MOUSE_STATE_SE:
      if(r){
        auto diff = MousePos - r->SE();
        r->Size.x += diff.x;
        r->Size.y += diff.y;
      }
      break;
    case MOUSE_STATE_SW:
      if(r){
        auto diff = MousePos - r->SW();
        r->Position.x += diff.x;
        r->Size.x -= diff.x;
        r->Size.y += diff.y;
      }
      break;
    case MOUSE_STATE_NE:
      if(r){
        auto diff = MousePos - r->NE();
        r->Position.y +=diff.y;
        r->Size.y -=diff.y;
        r->Size.x +=diff.x;
      }
      break;
    case MOUSE_STATE_NW:
      if(Re->SelectedObject){
        auto diff = MousePos - r->NW();
        r->Position = r->Position + diff;
        r->Size = r->Size - diff;
      }
      break;
    case MOUSE_STATE_HAND:
      if(r){
        SDL_Point center = {
          r->Position.x + r->Size.x/2,
          r->Position.y + r->Size.y/2
          };
          auto diff = MousePos - center;
          double angle;
          if(!diff.y){
            angle = (diff.x)? 90 : 270;
          }else{
          angle = std::atan((double)((double)-diff.x/(double)diff.y))*RAD2DEG;
          angle -= (diff.y > 0)? 180 : 0;
          }
          int i = std::round(angle / 45);
          r->Angle = i*45;
      }
    default:
      break;
    }
    //UpdateObject(r);
}

void Controller::HandleLineDrag(){
  auto l = (RenderLine*)Re->SelectedObject;
  switch(mstate){
    case MOUSE_STATE_HAND:{
      if(ManhattanDistance(MousePos, l->Start) < ManhattanDistance(MousePos, l->End)){
        l->Start = MousePos;
      }
      else{
        l->End = MousePos;
      }
      break;
    }
    case MOUSE_STATE_ARROW: {
      auto diff = DragOffset - MousePos;
      l->Start = l->Start + diff;
      l->End = l->End + diff;
      DragOffset = MousePos;
      break;
    }
    case MOUSE_STATE_NE:{}
    case MOUSE_STATE_NW:{}
    case MOUSE_STATE_SE:{}
    case MOUSE_STATE_SW:{}
    default:
      break;
  }
  //UpdateObject(l);
}

void Controller::HandleMouse(){
  SDL_FreeCursor(cursor);
  switch (sstate)
  {
      case SYSTEM_STATE_DRAWING:{ break; }
      case SYSTEM_STATE_SELECTION:{
          if(!ActiveDrag){
              if(Re->SelectedObject){
                  mstate = Re->decorator.GetCursor(MousePos);
                  switch(mstate){
                      case MOUSE_STATE_HAND:
                          cursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_HAND);
                          break;
                      case MOUSE_STATE_NE:
                          cursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENESW);
                          break;
                      case MOUSE_STATE_NW:
                          cursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENWSE);
                          break;
                      case MOUSE_STATE_SE:
                          cursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENWSE);
                          break;
                      case MOUSE_STATE_SW:
                          cursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENESW);
                          break;
                      case MOUSE_STATE_ARROW:
                          cursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_ARROW);
                      default:
                          break;
                  }
              }
              else{
                  cursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_ARROW);
              }
          }
          SDL_SetCursor(cursor);
          break;
      }
      default:{ break; }
  }
}

void Controller::Update(){
  SDL_PumpEvents();
  SDL_GetMouseState(&MousePos.x, &MousePos.y);
  // TODO: Add multiple Visibility Layer
  Layer* l = Layers[4];
  auto vision = (VisibilityLayer*)( l );
  // TODO: Change to player Token
  //vision->point = MousePos;
  /*
 ** For each Token that user_id == active_user.id
  ** Add Vision Point
  **
  */
  if (vision != NULL) {
    vision->visionPoints.clear();
  }
  // Only if active_user.flags & DM
  // TODO: Activate player actions
  // EXPERIMENT ONLY TOKENS

  for (auto tkn=tokens.begin(); tkn!=tokens.end(); ++tkn) {
    if ((*RoMap).count(tkn->ro_id)) {
      RenderTexture* ro = (RenderTexture *)((*RoMap)[tkn->ro_id]);
      if (ro != NULL && !ro->removed) {
        vision->visionPoints.push_back(ro->Center());
      }
    }
  }


//  if(spectator){
//    Re->Render();
//      return;
//  }
  HandleMouse();
  HandleEvents();
  if(activeLine) {
    activeLine->End = MousePos;
  }
  if(ActiveDrag){
      switch(Re->SelectedObject->Type){
          case RENDER_TYPE_TEXTURE:{
              HandleTextureDrag();
              break;
          }
          case RENDER_TYPE_LINE:{
              HandleLineDrag();
              break;
          }
          default:{
              break;
          }
      }
  }
  if(activePlayer.flags == PLAYER_FLAG_DM){
    //TODO: CHECK ALL VISIBILITY LAYERV
#ifdef __EMSCRIPTEN__
    if (Layers[4]->alpha > 100){
      Layers[4]->alpha = 100;
    }
#endif
  }
  Re->Render();
}

void Controller::HandleEvents(){
  SDL_Event e;
  while(SDL_PollEvent(&e)){
    HandleEvent(e);
  }
}

void Controller::MouseReleasedTexture(){
  SDL_Point mp;
  SDL_Point diff;
  RenderTexture* rt = (RenderTexture*)Re->SelectedObject;
  switch(mstate){
    case MOUSE_STATE_ARROW: {
      rt->Position = AproxToAnchor(rt->Position, _GridConfig);
      UpdateObject(Re->SelectedObject);
      break;
    }
    case MOUSE_STATE_SE: {
      diff = AproxToAnchor(MousePos, _GridConfig) - rt->SE();
      rt->Size.x += diff.x;
      rt->Size.y += diff.y;
      //Re->SelectedObject->Size =  AproxToAnchor(MousePos, _GridConfig) - Re->SelectedObject->Position;
      rt->Size = {
      std::max(rt->Size.x, _GridConfig.TileSize.x),
      std::max(rt->Size.y, _GridConfig.TileSize.y)
    };
      break;
    }
    case MOUSE_STATE_SW: {
      mp = AproxToAnchor(MousePos, _GridConfig);
      diff = mp - rt->SW();
      rt->Position.x += diff.x;
      rt->Size.x -= diff.x;
      rt->Size.y += diff.y;;
      rt->Size = {
      std::max(rt->Size.x, _GridConfig.TileSize.x),
      std::max(rt->Size.y, _GridConfig.TileSize.y)
    };
      break;
    }
    case MOUSE_STATE_NW: {
      mp = AproxToAnchor(MousePos, _GridConfig);
      diff = MousePos - rt->NW();
      rt->Position = rt->Position + diff;
      rt->Size = rt->Size - diff;
      rt->Size = {
      std::max(rt->Size.x, _GridConfig.TileSize.x),
      std::max(rt->Size.y, _GridConfig.TileSize.y)
    };
      break;
    }
    case MOUSE_STATE_NE: {
      mp = AproxToAnchor(MousePos, _GridConfig);
      diff = mp - rt->NE();
      rt->Position.y +=diff.y;
      rt->Size.y -=diff.y;
      rt->Size.x +=diff.x;;
      rt->Size = {
      std::max(rt->Size.x, _GridConfig.TileSize.x),
      std::max(rt->Size.y, _GridConfig.TileSize.y)
    };
      break;
    }
    default:{
      break;
    }
  }
}

void Controller::MouseReleasedLine(){
    if (ActiveDrag){
      UpdateObject(Re->SelectedObject);
    }
}

void Controller::MouseRelease(){
  if(Re->SelectedObject == NULL ){
    return;
  }
  //TODO: INYECT CHANGE OF STATE HERE
  switch(Re->SelectedObject->Type){
    case RENDER_TYPE_TEXTURE: {
      MouseReleasedTexture();
      break;
    }
    case RENDER_TYPE_LINE: {
      MouseReleasedLine();
      break;
    }
    default: {
      // TODO: Throw Error Unrecognized or Unhandled Object Type
      break;
    }
  }
  //UpdateObject(Re->SelectedObject);
}

void Controller::SelectionEvent(SDL_Event e){
    switch(e.type) {
      case SDL_MOUSEBUTTONDOWN: {
        switch(e.button.button){
          case SDL_BUTTON_LEFT:{
            if(mstate == MOUSE_STATE_ARROW){
              SelectRO();
            } else {
              ActiveDrag = true;
            }
            break;
          }
          case SDL_BUTTON_RIGHT: {
            SendRightClickEvent();
            break;
          }
          case SDL_BUTTON_MIDDLE: {
            /* Fallthrough */
          }
          default:{
            break;
          }
        }
        break;
        }
        case SDL_MOUSEBUTTONUP:{
            MouseRelease();
            ActiveDrag = false;
            mstate = MOUSE_STATE_ARROW;
            break;
        }
        case SDL_KEYDOWN:{
            HandleKeyboardSelectionEvent(e);
        }
        default: {
            break;
        }
  }
}

void Controller::DrawingEvent(SDL_Event e){
  switch(e.type) {
    case SDL_MOUSEBUTTONDOWN:{
      if (!activeLine){
        NewLine();
      }
      else{
        EndLine();
      }
      break;
    }
    case SDL_KEYDOWN: {
      switch(e.key.keysym.sym){
        case SDLK_ESCAPE:{
          if(activeLine) {
            Re->RemoveRO(activeLine);
            (*RoMap).erase(-1);
            activeLine = NULL;
          }
        }
        default: {
          break;
        }
      }
    }
    default: {
      break;
    }
  }
}

void Controller::HandleEvent(SDL_Event e){
    switch(sstate) {
        case SYSTEM_STATE_DRAWING: {
            DrawingEvent(e);
            break;
        }
        case SYSTEM_STATE_SELECTION: {
            SelectionEvent(e);
            break;
        }
        default: {
            break;
        }
    }
}

void Controller::ChangeState(SystemState state){
    switch(state) {
        case SYSTEM_STATE_DRAWING: {
            //std::cout << "Change to Drawing State" << std::endl;
            sstate = SYSTEM_STATE_DRAWING;
            Re->ClearSelectedObject();
            break;
        }
        case SYSTEM_STATE_SELECTION: {
            //std::cout << "Change to Selection State" << std::endl;
            if(activeLine) Re->RemoveRO(activeLine);
            activeLine = NULL;
            (*RoMap).erase(-1);
            sstate = SYSTEM_STATE_SELECTION;
            break;
        }
        default: {
            break;
        }
    }
}

int Controller::AddRo(int TextureID){
    Texture* _text = Re->TM->GetTexture(TextureID);
    RenderTexture* ro;
    int x,y;
    if (!_text) {
      // TODO Throw Exception, Texture doesn't exist
      return 0;
    }
    int _id = GetNewRoID();

    ro = new RenderTexture();
    ro->TextureID = _text->id;
    ro->id = _id;
    SDL_QueryTexture(_text->txt_ptr, NULL, NULL, &x, &y);
    ro->Size = {
    std::min(x, _GridConfig.TileSize.x*6),
    std::min(y, _GridConfig.TileSize.y*6)
};
    ro->Position = {
    std::max(1, MousePos.x - (ro->Size.x/2)),
    std::max(1, MousePos.y - (ro->Size.y/2))
};
    ro->Z = 1;
    ro->Type = RENDER_TYPE_TEXTURE;
    Re->AddRoToActiveLayer(ro);
    (*RoMap)[_id] = ro;

    /* Send Update */
    UpdateObject(ro);

    return 1;
}

int Controller::CopySelectedRo(){
    if(RoInClipoard && RoInClipoard->Type == RENDER_TYPE_TEXTURE){
        return AddRo(((RenderTexture*)RoInClipoard)->TextureID);
    }
    return 0;
}

int Controller::RemoveSelectedRo(){
    if(Re->SelectedObject){
        /* Remove from ROS; */
        auto ro = Re->SelectedObject;
        ro->removed = true;
        Re->RemoveRO(Re->SelectedObject);
        Re->ClearSelectedObject();
        UpdateObject(ro);
        return 1;
    }
    else {
        /* Nothing to remove */
        return 1;
    }
}

void Controller::HandleKeyboardSelectionEvent(SDL_Event e){
  RenderObject* ro;
  auto key = e.key;
  if(key.keysym.sym == SDLK_c && key.keysym.mod & KMOD_CTRL){
    this->CopySelected();
  }
  else if(key.keysym.sym == SDLK_v && key.keysym.mod & KMOD_CTRL){
    /* Makes a copy of the RO in "Clipboard" */
    CopySelectedRo();
    //std::cout << "Paste RO" << std::endl;
  }
  else if(key.keysym.sym == SDLK_ESCAPE){
    /* Clear out Clipboard */
    Re->ClearSelectedObject();
    RoInClipoard = NULL;
  }
  else if(key.keysym.sym == SDLK_BACKSPACE || key.keysym.sym == SDLK_KP_BACKSPACE || key.keysym.sym == SDLK_DELETE){
    this->RemoveSelected();
  }
}

int Controller::SwitchLayer(int LayerID){
  // TODO Assert or try catch
  if (Layers.count(LayerID)){
    Re->ActivateLayer(Layers[LayerID]);
    return 1;
  } else {
    return 0;
  }
}

int Controller::SwitchLayer(std::string name){
    if(LayerNameToID.count(name)){
        return SwitchLayer(LayerNameToID[name]);
    }
    return 0;
}

void Controller::PrintState(){
    std::cout << "{"
              << "layers: "

              << "}"
              << std::endl;
}

void Controller::CreateEmptySystem(){
    Layers[1] = new Layer();
    Layers[1]->id = 1;
    Layers[1]->status |= LAYER_STATUS_ACTIVE;
    LayerNameToID["background"] = 1;
    Re->layers.push_back(Layers[1]);
    Layers[2] = new Layer();
    Layers[2]->id = 2;
    Layers[2]->status |= LAYER_STATUS_ACTIVE;
    LayerNameToID["token"] = 2;
    Re->layers.push_back(Layers[2]);
    Layers[3] = new Layer();
    Layers[3]->id = 3;
    Layers[3]->status |= LAYER_STATUS_ACTIVE;
    LayerNameToID["overlay"] = 3;
    Re->layers.push_back(Layers[3]);
#ifdef __EMSCRIPTEN__
    // TODO FIX VISIBILITY LAYER ON BINARY
    // meanwhile have it only on the WASM build
    Layers[4] = new VisibilityLayer();
    Layers[4]->id = 4;
    Layers[4]->status |= LAYER_STATUS_ACTIVE;
    LayerNameToID["vision"] = 4;
    Re->layers.push_back(Layers[4]);
#endif
    Re->ActivateLayer(Layers[2]);
    Re->GenLayers();
}

int Controller::NewImage(std::string filename){
  SDL_Surface* image = IMG_Load(&filename[0]);
  if (!image) {
    std::cerr << "IMG LOAD: " << IMG_GetError() << std::endl;
    return 0;
  }
  auto txt = SDL_CreateTextureFromSurface(Re->windowData.renderer, image);
  Texture* _Texture = Re->TM->AddTexture(txt, filename);
  SDL_FreeSurface(image);
  return _Texture->id;
}

int Controller::SetRO(int id, int index, int layer, int idTexture){
  if((*RoMap).count(id)){
    //VERBOSE
    std::cerr << "ERROR: RO ID ALREADY EXIST" << std::endl;
    return 0;
  }
  if(!Layers.count(layer)){
    //VERBOSE
    std::cerr << "ERROR: LAYERID DOESN'T EXIST" << std::endl;
    return 0;
  }
  if(!Re->TM->TextureMap.count(idTexture)){
    std::cerr << "ERROR: TEXTURE ID DOESN'T EXIST" << std::endl;
    return 0;
  }
  RenderTexture* re = new RenderTexture();
  re->id = id;
  (*RoMap)[id] = re;
  Layer* l = Layers[layer];
  re->layer=layer;
  re->TextureID = idTexture;
  l->ros.insert(l->ros.begin() + index, re->id);
  int x, y;
  SDL_QueryTexture(Re->TM->GetTexture(idTexture)->txt_ptr, NULL, NULL, &x, &y);
  re->Size = {
  std::min(x, _GridConfig.TileSize.x*6),
  std::min(y, _GridConfig.TileSize.y*6)
};
  re->Position = {
  std::max(1, MousePos.x - (re->Size.x/2)),
  std::max(1, MousePos.y - (re->Size.y/2))
};
  re->Z = 1;
  re->Type = RENDER_TYPE_TEXTURE;
  return re->id;
}

int Controller::GetNewRoID(){
  while((*RoMap).count(roid)){
    roid++;
  }
  return roid;
}

int Controller::ResizeRO(int roid, SDL_Point p){
  if(!romap.count(roid)){
    std::cerr << "ERROR: ROID DOESN'T EXIST" << std::endl;
    return 0;
  }
  RenderTexture* re = (RenderTexture*)(*RoMap)[roid];
  re->Size = {
  std::min(p.x, _GridConfig.TileSize.x*6),
  std::min(p.y, _GridConfig.TileSize.y*6)
};
  re->Size = {
  std::max(re->Size.x, _GridConfig.TileSize.x),
  std::max(re->Size.y, _GridConfig.TileSize.y)
};
  return 1;
}

int Controller::PosRO(int roid, SDL_Point p){
  if(!(*RoMap).count(roid)){
    std::cerr << "ERROR: RenderObject ID DOESN'T EXIST" << std::endl;
    return 0;
  }
  RenderTexture* re = (RenderTexture*)(*RoMap)[roid];
  re->Position = p;
  return 1;
}

void Controller::VisibilityMode(){
  Re->ActivateLayer(Layers[4]);
  ChangeState(SYSTEM_STATE_DRAWING);
}

int Controller::SerializedUpdate(std::string serialized){
  int res;
  try{
    SerializedObj so = Deserialize<SerializedObj>(serialized);
    switch(so.type){
      case SERIAL_TYPE_RENDER_TEXTURE: {
        res = SerializedUpdateRenderTexture(so.archive);
        break;
      }
      case SERIAL_TYPE_RENDER_LINE: {
        res = SerializedUpdateRenderLine(so.archive);
        break;
      }
      case SERIAL_TYPE_PLAYER: {
        res = SerializedUpdatePlayer(so.archive);
        break;
      }
      case SERIAL_TYPE_LAYER: {
        res = SerializedUpdateLayer(so.archive);
        break;
      }
      case SERIAL_TYPE_TOKEN: {
        res = SerializedUpdateToken(so.archive);
        break;
      }
      default: {
        break;
      }
    }
  }catch(std::exception& e){
    std::cerr << "Exception Recovering Serialized Object: " << e.what() << std::endl;
    std::cerr << serialized << std::endl;
    return 0;
  }
  return res;
}

int Controller::SerializedUpdatePlayer(std::string serialized){
  int res;
  try{
    Player p = Deserialize<Player>(serialized);
    /* Seek out player */
    Player* p2 = NULL;
    for(auto p_stored = this->players.begin(); p_stored != this->players.end(); ++p_stored){
      if(p_stored->id == p.id){
        p2 = &(*p_stored);
        break;
      }
    }
    if (p2) { /* Update Player */
      // VERBOSE
      p2->name = p.name;
      p2->flags = p.flags;
    } else { /* New Playerr */
      p2 = new Player();
      p2->id = p.id;
      p2->name = p.name;
      p2->flags = p.flags;
    }
    return 1;
  } catch (std::exception& e) {
    std::cerr << "Exception Recoverign Render Texture: " << e.what() << std::endl;
    return 0;
  }
}

int Controller::SerializedUpdateLayer(std::string serialized) {
  try{
    Layer l = Deserialize<Layer>(serialized);
    Layer* updatedLayer;
    switch(l.layer_type){
      case LAYER_TYPE_VISIBILITY_LAYER:{
        VisibilityLayer vl = Deserialize<VisibilityLayer>(serialized);
        updatedLayer = &vl;
        break;
      }
      default: {
        updatedLayer = &l;
        break;
      }
    }
    /* Seek out Layer */
    if(this->Layers.count(l.id)){
      /* Update */
      Layer* localLayer = Layers[updatedLayer->id];
      localLayer->alpha = updatedLayer->alpha;
      localLayer->layer_type = updatedLayer->layer_type;
      localLayer->status = updatedLayer->status;
      // TODO: Change into a checking roll
      localLayer->ros.clear();
      for (auto roid=updatedLayer->ros.begin(); roid!=updatedLayer->ros.end(); ++roid) {
        localLayer->ros.push_back(*roid);
      }
      //if ( updatedLayer->layer_type == LAYER_TYPE_VISIBILITY_LAYER ) {
        //VisibilityLayer* vl = static_cast<VisibilityLayer*>(updatedLayer);
        //vl->visionPoints.clear();
      //}
    } else {
      /* New Layer */
      Layer* newLayer;
      switch(l.layer_type){
        case LAYER_TYPE_VISIBILITY_LAYER: {
          newLayer = new VisibilityLayer();
          break;
        }
        case LAYER_TYPE_TEXTURE_LAYER: {

        }
        default: {
          newLayer = new Layer();
          break;
        }
      }
      newLayer->id = l.id;
      newLayer->alpha = l.alpha;
      newLayer->status = l.status;
      for (auto roid=l.ros.begin(); roid!=l.ros.end(); ++roid) {
        newLayer->ros.push_back(*roid);
      }
      Re->InitLayer(newLayer);
    }
  } catch ( std::exception& e ) {
    std::cerr << "Exception Updating Layer: "
              << e.what() << std::endl;
    return 0;
  }

  return 1;
}

void Controller::ChangeToLayer(int roID, int layerID, bool update){
  if (!(*RoMap).count(roID) || !Layers.count(layerID)) {
    return;
  }
  RenderObject* ro = (*RoMap)[roID];
  if(ro->layer == layerID){
    return;
  }
  /* We Move the Ro away from the work layer, we're still on the original layer */
  if (Re->SelectedObject && Re->SelectedObject->id == roID){
    Re->ClearSelectedObject();
  }
  Layer* l = Layers[ro->layer];
  l->RemoveRo(roID);
  ro->layer = layerID;
  Layers[layerID]->ros.push_back(roID);
  if(update) UpdateObject(ro);
}

int Controller::SerializedUpdateRenderTexture(std::string serialized){
  try{
    RenderTexture re = Deserialize<RenderTexture>(serialized);
    if((*RoMap).count(re.id)){
      /* Update */
      RenderObject* ro = (*RoMap)[re.id];
      if(ro->Type != re.Type){
        std::cerr << "Different type of RenderObject" << std::endl;
        std::cerr << ro->Type << " vs " << re.Type << std::endl;
        std::cerr << serialized << std::endl;
        return 0;
      }
      RenderTexture* rt = (RenderTexture*)ro;
      if(rt->removed){
        return 1;
      }
      rt->Z = re.Z;
      rt->removed = re.removed;
      rt->Angle = re.Angle;
      rt->Position = re.Position;
      rt->Size = re.Size;
      rt->TextureID = re.TextureID;
      if(!rt->removed) ChangeToLayer(re.id, re.layer, false);
    }
    else{
      /* New RO */
      CopyRT(re);
    }
  }catch(std::exception& e){
    std::cerr << "Exception Recovering Render Texture: " << e.what() << std::endl;
    return 0;
  }
  return 1;
}

int Controller::CopyRT(RenderTexture re, bool update){
  RenderTexture* rt = new RenderTexture();
  (*RoMap)[re.id] = rt;
  rt->id = re.id;
  rt->layer = -1;
  rt->Z = re.Z;
  rt->Type = re.Type;
  rt->removed = false;
  ChangeToLayer(re.id, re.layer, false);
  rt->Angle = re.Angle;
  rt->Position = re.Position;
  rt->Size = re.Size;
  rt->TextureID = re.TextureID;
  if(update) UpdateObject(rt);
  return 1;
}

int Controller::SerializedUpdateRenderLine(std::string serialized){
  try{
    RenderLine re = Deserialize<RenderLine>(serialized);
    if((*RoMap).count(re.id)){
      /* Update */
      RenderObject* ro = (*RoMap)[re.id];
      if(ro->Type != re.Type){
        std::cerr << "Ro Types not the same" << std::endl;
        return 0;
      }
      RenderLine* rl = (RenderLine*) ro;
      if(rl->removed) {
        return 1;
      }
      rl->removed = re.removed;
      rl->Start = re.Start;
      rl->End = re.End;
      rl->Z = re.Z;
      rl->width = re.width;
      rl->color = re.color;
      if(!rl->removed) ChangeToLayer(rl->id, re.layer, false);
    }
    else{
      /* New RO */
      CopyRL(re);
    }
  }catch(std::exception& e){
    std::cerr << "Exception Recovering serialized renderline: " << e.what() << std::endl;
    return 0;
  }
  return 1;
}

int Controller::CopyRL(RenderLine re){
  RenderLine* rl = new RenderLine();
  (*RoMap)[re.id] = rl;
  rl->id = re.id;
  rl->Type = re.Type;
  rl->Start = re.Start;
  rl->End = re.End;
  rl->width = re.width;
  rl->color = re.color;
  rl->Z = re.Z;
  rl->layer = 0;
  rl->removed = re.removed;
  if (Layers.count(re.layer)) {
    rl->layer = re.layer;
    Layers[re.layer]->ros.push_back(rl->id);
  }
  return 1;
}

void Controller::PrintLayerState(){
  std::stringstream ss;
  LayerEditorHelper le;
  for(auto l : Re->layers){
    if(l->layer_type == LAYER_TYPE_TEXTURE_LAYER){
      le.Texture_layers.push_back(*l);
    }
    else{
      le.Vision_layers.push_back(*(dynamic_cast<VisibilityLayer*>(l)));
    }
  }
  {
    cereal::JSONOutputArchive archive(ss);
    archive(le);
  }

  std::string st = ss.str();
  st.erase(std::remove(st.begin(), st.end(), '\n'), st.end() );
  std::cout << st << std::endl;

}

void Controller::ChangeLineConfig(Uint32 color, Uint16 width){
  lineConfig.Color = color;
  lineConfig.Width = width;
}

void Controller::GenDemoGM(){
  Player gm;
  gm.id = 1;
  gm.flags = PLAYER_FLAG_DM;
  gm.name = "GM";
  this->players.push_back(gm);
}

void Controller::AddNewUser(int id, char* name, unsigned int flags){
  Player p;
// TODO CHECK ID;
  p.id = id;
  p.name = std::string(name);
  p.flags = flags;
  players.push_back(p);
}

int Controller::SwitchUser(int id){
  //TODO Change Players to a map for fast search
  for (auto p=players.begin(); p!=players.end(); ++p) {
    if(p->id == id){
      activePlayer = *p;
      return 1;
    }
  }
  return 0;
}

int Controller::ModifyUser(int id, char* name, unsigned int flags){
  for (auto p=players.begin(); p!=players.end(); ++p) {
    if(p->id == id){
      if(name != NULL){
        p->name = name;
      }
      if(flags != PLAYER_FLAG_NO_FLAG){
        p->flags = flags;
      }
      UpdateObject(&(*p));
      return 1;
    }
  }
  return 0;
}

int Controller::SerializedUpdateToken(std::string serialized){
  try{
    Token t = Deserialize<Token>(serialized);
    Token* localToken = NULL;
    for( auto tkn = this->tokens.begin(); tkn != this->tokens.end(); ++tkn ){
      if(t.id == tkn->id){
        localToken = &(*tkn);
        break;
      }
    }
    if( localToken == NULL ){
      localToken = new Token();
      localToken->id = t.id;
      localToken->ro_id = t.ro_id;
      localToken->user_id = t.user_id;
      this->tokens.push_back(*localToken);
    }
    localToken->ro_id = t.ro_id;
    localToken->user_id = t.user_id;
  } catch ( std::exception &e ){
    std::cerr << "Exception Deserialing Token update: " << e.what()
      << std::endl;
    return 0;
  }
  return 1;
}

int Controller::MakeToken(int ro_id, int user_id){
  Token t;
  t.ro_id = ro_id;
  t.user_id = user_id;
  t.id = tokens.size();
  tokens.push_back(t);
  Token* to = &tokens.back();
  UpdateObject(to);
  return 1;
}

void Controller::SendRightClickEvent() {
  RightClickStatus s;
  s.roSelected = Re->SelectedObject != NULL;
  s.layerId = Re->activeLayer->id;
  s.systemState = this->sstate;
  if (s.roSelected) {
    s.ro = Re->SelectedObject;
  }
  else {
    s.ro = new RenderObject();
  }
  std::string message = MakeJson<RightClickStatus>(s);
  SendRightClick(message);
  if (! s.roSelected) {
    delete s.ro;
  }
}

void Controller::RemoveRo(int ro_id){
  if( (*RoMap).count(ro_id) ){
    auto ro = (*RoMap)[ro_id];
    bool modified = ro->removed != true;
    ro->removed = true;
    // TODO REMOVE TOKEN
    if (Layers.count(ro->layer)) {
      auto l = Layers[ro->layer];
      l->RemoveRo(ro_id);
    }
    if (Re->SelectedObject != NULL && Re->SelectedObject->id == ro_id){
      Re->ClearSelectedObject();
    }
    UpdateObject(ro);
  }
}

void Controller::RemoveSelected() {
    if(Re->SelectedObject){
      RemoveRo(Re->SelectedObject->id);
    }
}

void Controller::CopySelected(){
    /* Puts Ro pointer into "Clipboard" */
    RoInClipoard = Re->SelectedObject;
    //std::cout << "RO IN CLIPBOARD"<< std::endl;
}

void Controller::PasteFromClipBoard(){
  this->CopySelectedRo();
}

int Controller::AddRoIn(int TextureID, int x, int y) {
    Texture* _text = Re->TM->GetTexture(TextureID);
    RenderTexture* ro;
    int _x, _y;
    if (!_text) {
      // TODO Throw Exception, Texture doesn't exist
      return 0;
    }
    int _id = GetNewRoID();

    ro = new RenderTexture();
    ro->TextureID = _text->id;
    ro->id = _id;
    SDL_QueryTexture(_text->txt_ptr, NULL, NULL, &_x, &_y);
    ro->Size = {
    std::min(_x, _GridConfig.TileSize.x*6),
    std::min(_y, _GridConfig.TileSize.y*6)
};
    ro->Position = {
    std::max(1, x - (ro->Size.x/2)),
    std::max(1, y - (ro->Size.y/2))
};
    ro->Z = 1;
    ro->Type = RENDER_TYPE_TEXTURE;
    Re->AddRoToActiveLayer(ro);
    (*RoMap)[_id] = ro;

    /* Send Update */
    UpdateObject(ro);

    return 1;
}

int Controller::InsertImage(std::string filename, int id){
  SDL_Surface* image = IMG_Load(&filename[0]);
  if (!image) {
    std::cerr << "IMG LOAD: " << IMG_GetError() << std::endl;
    return 0;
  }
  auto txt = SDL_CreateTextureFromSurface(Re->windowData.renderer, image);
  Texture* _Texture = Re->TM->AddTextureWithID(txt, filename, id);
  SDL_FreeSurface(image);
  return _Texture->id;
}

bool Controller::TextureInTokens(RenderObject* ro) {
  for(auto tkn = tokens.begin(); tkn != tokens.end(); ++ tkn) {
    if(tkn->ro_id == ro->id) {
      return true;
    }
  }
  return false;
}
