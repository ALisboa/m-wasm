#include "Grid.hpp"
#include <cmath>
#include <stdexcept>
#include <iostream>

int Round(float x){
  int base = std::floor(x);
  return (x-base > .5f) ? base+1 : base;
}

SDL_Point CloseCornerAnchor(SDL_Point position, SDL_Point size) {
  float x = (float)position.x/(float)size.x, y = (float)position.y/(float)size.y;
  return {Round(x)*size.x,Round(y)*size.y};
}

SDL_Point AproxToAnchor(SDL_Point pos, GridConfig config){
  SDL_Point closesAnchor;
  switch(config.Anchors){
    case CORNER: {
      return CloseCornerAnchor(pos, config.TileSize);
      break;
    }
    default: {
      throw std::invalid_argument("Anchor type not implemented");
    }
  }
  return {0, 0};
}

SDL_Point TopLeftCorner(SDL_Point pos, SDL_Point size){
  return { (int)(std::floor(pos.x/size.x)*size.x) , (int)(std::floor(pos.y/size.y)*size.y) };
}
