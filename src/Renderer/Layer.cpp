#include "Layer.hpp"
#include <SDL2/SDL2_gfxPrimitives.h>
#include <set>
#include "../MathHelper.hpp"
#include <algorithm>
#include <unordered_set>
#include <stdexcept>


Layer::Layer(){
	alpha = 255;
	status = 0;
	layer_type = LAYER_TYPE_TEXTURE_LAYER;
	texture = NULL;
}

Layer::~Layer() {
	
}

VisibilityLayer::VisibilityLayer() : Layer(){
	layer_type = LAYER_TYPE_VISIBILITY_LAYER;
	aux_texture = NULL;
}

VisibilityLayer::~VisibilityLayer(){
}

void Layer::Render(SDL_Renderer* renderer){
	//if( status & LAYER_STATUS_ACTIVE){
		UpdateTexture(renderer);
	//}
	SDL_SetTextureAlphaMod(texture, alpha);
	SDL_RenderCopy(renderer, texture, NULL, NULL);
}


void Layer::ClearTexture(SDL_Renderer* renderer, SDL_Texture* tx,
						 Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha){
	SDL_SetRenderTarget(renderer, tx);
	SDL_SetTextureBlendMode(tx, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(renderer, red, green, blue, alpha);
	SDL_RenderClear(renderer);
}

void Layer::UpdateTexture(SDL_Renderer* renderer){
	ClearTexture(renderer, texture);
	//SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_NONE);
	for(auto ro: ros){
		//ro->Render(renderer);
		RenderObject* renderobject = (*RoMap)[ro];
		if (renderobject != NULL && renderobject->removed != true) {
			renderobject->Render(renderer);
		}
	}
	SDL_SetRenderTarget(renderer, NULL);
}

void VisibilityLayer::Render(SDL_Renderer *renderer){
	//if( status & LAYER_STATUS_ACTIVE){
		UpdateTexture(renderer);
	//}
	SDL_SetTextureAlphaMod(texture, alpha);
	//SDL_SetTextureColorMod(texture, 0, 0, 0);
	//SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_NONE);
	SDL_RenderCopy(renderer, texture, NULL, NULL);
}

void VisibilityLayer::MakeAuxTexture(SDL_Renderer *renderer){
	int x, y;
	SDL_QueryTexture(texture, NULL, NULL, &x, &y);
	//TODO: CHECK SIZE OF SHADOW TEXTURE

	if ( aux_texture == NULL ){
		aux_texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_TARGET, x, y);
	}
}

void VisibilityLayer::DrawSightPolygons(SDL_Renderer* renderer, std::vector<Ray> segments){

	for(auto point : visionPoints){
		std::vector<PolyPoint> poly = GenSightPoly({(double)point.x, (double)point.y}, segments);
		std::vector<Sint16>xs;
		std::vector<Sint16>ys;
		for(auto p : poly){
			xs.push_back(p.x);
			ys.push_back(p.y);
		}
		/*
		 * filledPolygonRGBA uses SDL_BLENDMODE_BLEND on A != 255, SDL_BLENDMODE_NONE otherwise
		 */
		filledPolygonRGBA(renderer, &xs[0], &ys[0], xs.size(), 0x00, 0x00, 0x00, 0xFF);
	}
}

void VisibilityLayer::GenSegments(std::vector<Ray> &segments){
	int x,y;
	SDL_QueryTexture(texture, NULL, NULL, &x, &y);

	segments.push_back({{-1, -1}, {(double)x, (double)-1}});
	segments.push_back({{(double)x, (double)-1}, {(double)x, (double)y}});
	segments.push_back({{(double)x, (double)y}, {(double)-1, (double)y}});
	segments.push_back({{(double)-1, (double)y}, {(double)-1, (double)-1}});
	for(auto roid: ros){
		RenderObject* ro = (*RoMap).count(roid) ? (*RoMap)[roid] : NULL ;
		if(ro && ro->removed == false && ro->Type == RENDER_TYPE_LINE){
			RenderLine* rl = (RenderLine*)ro;
			segments.push_back({{(double)rl->Start.x, (double)rl->Start.y}, {(double)rl->End.x, (double)rl->End.y}});
		}
	}
}

void VisibilityLayer::UpdateTexture(SDL_Renderer *renderer){
	ClearTexture(renderer, texture, 0x00, 0x00, 0x00, 0xFF);
	MakeAuxTexture(renderer);

	std::vector<Ray> segments;
	this->GenSegments(segments);
	ClearTexture(renderer, aux_texture, 0x00, 0x00, 0x00, 0x00);

	DrawSightPolygons(renderer, segments);


	if(BLENDMODE_AMINUS != SDL_BLENDMODE_INVALID){
		auto a = SDL_SetTextureBlendMode(aux_texture, BLENDMODE_AMINUS);
		//TODO: DM FOG VISION
		if( a < 0){
			//throw std::invalid_argument("Error Here");
#ifdef __EMSCRIPTEN__
			std::cerr << "Error setting blend mode to visibility layer's aux texture" << std::endl;
			std::cerr << SDL_GetError() << std::endl;
#endif
			SDL_SetTextureBlendMode(aux_texture, SDL_BLENDMODE_BLEND);
			ClearTexture(renderer, aux_texture);
			ClearTexture(renderer, texture);
		}
	}

	SDL_SetRenderTarget(renderer, texture);
	SDL_RenderCopy(renderer, aux_texture, NULL, NULL);
	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
	if(status & LAYER_STATUS_ACTIVE){
		for(auto roid: ros){
			if((*RoMap).count(roid)){
				RenderObject* ro = (*RoMap)[roid];
				if(ro->removed == false) {
					ro->Render(renderer);
				}
			}
		}
	}
	SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
	SDL_SetRenderTarget(renderer, NULL);
	//TEMP
	//SDL_DestroyTexture(aux_texture);
	//aux_texture = NULL;
}

void Layer::RemoveRo(int id){
	ros.erase(std::remove(ros.begin(), ros.end(), id), ros.end());
}
