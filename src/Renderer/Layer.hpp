#ifndef RENDERLAYER_H
#define RENDERLAYER_H
#include "RenderObject.hpp"
#include "RenderLine.hpp"
#include "types.hpp"
#include <SDL2/SDL.h>
#include <vector>
#include "../MathHelper.hpp"
#include <memory>

extern std::shared_ptr<std::map<int, RenderObject*>> RoMap;

enum LAYER_STATUS {
	LAYER_STATUS_INITIALLIZED = 1,
	LAYER_STATUS_ACTIVE = 1<<1,
	LAYER_STATUS_VISIBLE = 1<<2,
	LAYER_STATUS_MODIFIED = 1 << 3,
};

enum LAYER_TYPE {
	LAYER_TYPE_TEXTURE_LAYER,
	LAYER_TYPE_VISIBILITY_LAYER
};

class Layer {
	public:
		//TODO: PLAYER INVISIBLE
		int id;
		unsigned int layer_type;
		unsigned int status;
		std::vector<int> ros;
		uint8_t alpha;
		virtual void Render(SDL_Renderer* renderer);
		Layer();
		~Layer();
		SDL_Texture* texture;
		void RemoveRo(int id);
	private:
		virtual void UpdateTexture(SDL_Renderer* renderer);
	protected:
		virtual void ClearTexture(SDL_Renderer* renderer, SDL_Texture* tx,
								  Uint8 red = 0x00, Uint8 green = 0x00,
								  Uint8 blue = 0x00, Uint8 alpha = 0x00);
};

class VisibilityLayer: public Layer{
	public:
		void Render(SDL_Renderer* renderer);
		std::vector<SDL_Point> visionPoints;
		VisibilityLayer();
		~VisibilityLayer();
	private:
		void UpdateTexture(SDL_Renderer* renderer);
		SDL_Texture* aux_texture;
		void MakeAuxTexture(SDL_Renderer* renderer);
		void DrawSightPolygons(SDL_Renderer* renderer, std::vector<Ray> segments);
		void GenSegments( std::vector<Ray>& segments);
};

#endif
