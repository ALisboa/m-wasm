#ifndef RENDERER_TYPES_H
#define RENDERER_TYPES_H
#include <SDL2/SDL.h>
#include <string>

class Texture{
  public:
    SDL_Texture* txt_ptr;
    std::string filename;
    uint id;
    Texture();
    ~Texture();
};

enum RenderType {
RENDER_TYPE_LINE,
RENDER_TYPE_TEXTURE
};


enum MouseState{
MOUSE_STATE_ARROW,
MOUSE_STATE_NW,
MOUSE_STATE_NE,
MOUSE_STATE_SW,
MOUSE_STATE_SE,
MOUSE_STATE_HAND,
};

#endif
