#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H
#include <SDL2/SDL.h>
#include <string>
#include <map>
#include "types.hpp"

class TextureManager {
  public:
    std::map<Uint32, Texture*> TextureMap;
    Texture* AddTexture(SDL_Texture* texture, std::string filename);
    Texture* AddTextureWithID(SDL_Texture* texture, std::string filename, int id);
    Uint32 SetTexture(Uint32 i, SDL_Texture* texture);
    Texture* NewEmptyTexture();
    Texture* GetTexture(Uint32 i);
    TextureManager();
    ~TextureManager();
  private:
    Uint32 nid;
};

#endif
