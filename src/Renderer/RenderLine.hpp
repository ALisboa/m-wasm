#ifndef __RENDERLINE_H_
#define __RENDERLINE_H_
#include "RenderObject.hpp"

class RenderLine : public RenderObject {
  public:
        Uint16 width;
        Uint32 color;
        SDL_Point Start;
        SDL_Point End;
        void Render(SDL_Renderer* renderer);
        void Resize(double ratio[2]);
        bool Inside(int x, int y);
        RenderLine();
};


#endif // __RENDERLINE_H_
