#ifndef RENDERER_H
#define RENDERER_H
#include "../Grid.hpp"
#include <SDL2/SDL.h>
#include <vector>
#include "TextureManager.hpp"
#include "types.hpp"
#include "RenderObject.hpp"
#include "RenderLine.hpp"
#include "Decorator.hpp"
#include "Layer.hpp"

struct WindowData {
    SDL_Window* window;
    SDL_Renderer* renderer;
    SDL_Point size;
};


struct RO_CMP {
    bool operator() (const RenderObject* ro1, const RenderObject* ro2) {
        return ro1->Z < ro2->Z;
    }
};

enum RenderState{
INIT, EMAP, PLAY, OVERLAYS, ELIGHT
};

class Renderer {
    public:
        Decorator decorator;
        GridConfig* GC;
        TextureManager* TM;
        Layer* activeLayer;
        std::vector<RenderObject*> ros;
        std::vector<Layer*> layers;
        WindowData windowData;
        void SetSelectedObject(RenderObject* ro);
        void ClearSelectedObject();
        void ResizeWindow(SDL_Point newSize);
        void changeState(RenderState s);
        void AddRO(RenderObject* ro);
        void RemoveRO(RenderObject* ro);
        bool AddToMapLayer(RenderObject* ro);
        bool AddToTokenLayer(RenderObject* ro);
        void InitRenderer();
        void Render();
        void UpdateGrid();
        void ResizeGrid(SDL_Point oldSize, SDL_Point newSize);
        RenderObject* CreateGrid(GridConfig gc);
        Renderer();
        ~Renderer();
        void Sort();
        void MoveToBack(RenderObject* ro);
        void MoveToBack(int index);
        void ActivateLayer(Layer* layer);
        void AddRoToActiveLayer(RenderObject* ro);
        void GenLayers();
        void InitLayer(Layer* l);
        RenderObject* SelectedObject;
        RenderTexture* Grid;
    private:
        RenderState State;
        bool FreeRenderObject(RenderObject* ro);
        void InitWindow();
        void InitSDLRenderer();
        void UpdateDecorator();
        void Render(RenderObject ro);
};


#endif
