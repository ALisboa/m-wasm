#include "Renderer.hpp"
#include <stdexcept>
#include <iostream>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <algorithm>
#include <cmath>
#include "../MathHelper.hpp"

#define RAND2DEGREE (float)57.295774

#define a(A) (0xFF000000&A)>>24
#define r(A) (0x00FF0000&A)>>16
#define g(A) (0x0000FF00&A)>>8
#define b(A) (0x000000FF&A)

bool CompareObj(RenderObject* a, RenderObject* b){
  return a->Z < b->Z;
}


Renderer::Renderer(){
  this->decorator.active = false;
  this->decorator.Color = 0xFF00FF00;
  this->decorator.base = nullptr;
  this->decorator.activeSize = 10;
}

Renderer::~Renderer(){
  for(auto ro : ros){
    free(ro);
  }
  SDL_DestroyRenderer(this->windowData.renderer);
  SDL_DestroyWindow(this->windowData.window);
}

void Renderer::InitWindow(){
  windowData.window = SDL_CreateWindow("V20T", SDL_WINDOWPOS_UNDEFINED, 
    SDL_WINDOWPOS_UNDEFINED, 1, 1, SDL_WINDOW_RESIZABLE);
  windowData.size = {1,1};
}

void Renderer::InitSDLRenderer(){
  windowData.renderer = SDL_CreateRenderer(windowData.window, 
    -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE);
}

void Renderer::ResizeWindow(SDL_Point size){
  //TODO: SANITY CHECK
  windowData.size = size;
	for(auto l : layers){
		SDL_DestroyTexture(l->texture);
		l->texture = nullptr;
		l->status &= !LAYER_STATUS_INITIALLIZED;
	}
  SDL_SetWindowSize(this->windowData.window, size.x, size.y);
	GenLayers();
}

void Renderer::InitRenderer(){
  InitWindow();
  InitSDLRenderer();
}

void Renderer::Render(){
  UpdateDecorator();
  SDL_SetRenderDrawColor(windowData.renderer, r(GC->BackgroundColor), g(GC->BackgroundColor), b(GC->BackgroundColor), a(GC->BackgroundColor));
  SDL_SetRenderDrawBlendMode(windowData.renderer, SDL_BLENDMODE_ADD);
  SDL_RenderClear(windowData.renderer);
  /* RENDER EVERYTHING IN ORDER */
  /* RENDER BY LAYER */
  for(Layer* layer : layers){
    layer->Render(windowData.renderer);
  }
  // Draw Active Layer
  /*
    for(auto ro : activeLayer->ros){
    ro->Render(windowData.renderer);
    }
  */

  if(Grid)
    Grid->Render(windowData.renderer);
  if(decorator.active){
    decorator.Render(windowData.renderer);
  }
  /* PRESENT TO WINDOW */
  SDL_RenderPresent(windowData.renderer);
}

void Renderer::Render(RenderObject ro){
  
}


bool Renderer::AddToMapLayer(RenderObject* ro){
  throw std::logic_error("Not Implemented");
  return 0;
}

bool Renderer::AddToTokenLayer(RenderObject* ro){
  throw std::logic_error("Not Implemented");
  return 0;
}

void changeState(RenderState s){
  
}

void Renderer::AddRO(RenderObject* ro){
  activeLayer->ros.push_back(ro->id);
}

void Renderer::RemoveRO(RenderObject* ro){
    for(int i = 0; i < activeLayer->ros.size(); ++i){
      if(activeLayer->ros[i] == ro->id){
        activeLayer->ros.erase(activeLayer->ros.begin() + i);
        return;
      }
    }
}

RenderObject* Renderer::CreateGrid(GridConfig gc){
  RenderTexture* grid = new RenderTexture();
	auto text = TM->NewEmptyTexture();
  grid->TextureID = text->id;
  grid->Z = -1;
  grid->Size = {gc.TileSize.x * gc.Size.x, gc.TileSize.y * gc.Size.y};
  grid->Position = {0,0};
  text->txt_ptr = SDL_CreateTexture(windowData.renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, grid->Size.x, grid->Size.y);
  SDL_SetRenderTarget(windowData.renderer, text->txt_ptr);
  SDL_SetRenderDrawColor(windowData.renderer,0, 0, 0, 0);
  SDL_RenderClear(windowData.renderer);
  SDL_SetTextureBlendMode(text->txt_ptr, SDL_BLENDMODE_BLEND);
  SDL_SetRenderDrawBlendMode(windowData.renderer, SDL_BLENDMODE_BLEND);

  for(size_t i = 0; i < gc.Size.x; i++){
    thickLineColor(windowData.renderer, i*gc.TileSize.x, 0, i*gc.TileSize.x, gc.Size.y*gc.TileSize.y, 
    gc.width, gc.GridColor);
  }
  for (size_t i = 0; i < gc.Size.y; i++)
  {
    thickLineColor(windowData.renderer, 0, i*gc.TileSize.y, gc.Size.x*gc.TileSize.x, i*gc.TileSize.y, gc.width, gc.GridColor);
  }

  SDL_SetRenderTarget(windowData.renderer, nullptr);
  return grid;
}


void Renderer::Sort(){
  std::sort(ros.begin(), ros.end(), CompareObj);
}

void Renderer::MoveToBack(RenderObject* ro){
  for(int i = 0; i < ros.size() ; ++i){
    if(ros[i] == ro){
      MoveToBack(i);
      break;
    }
  }
}

void Renderer::MoveToBack(int index){
  auto it = ros.begin() + index;
  std::rotate(it, it + 1, ros.end());
}

void Renderer::ClearSelectedObject(){
  if(decorator.active){
    decorator.base = nullptr;
    decorator.active = false;
  }
  SelectedObject = nullptr;
}

void Renderer::SetSelectedObject(RenderObject* ro){
  decorator.Generate(dynamic_cast<RenderTexture*>(ro));
  ClearSelectedObject();
  //MoveToBack(ro);
  SelectedObject = static_cast<RenderTexture*>(ro);
  decorator.active = true;
  //Sort();
  //Decorator = CreateDecorator();
}

void Renderer::UpdateDecorator(){
  if(!decorator.active){
    return;
  }
  decorator.Generate(SelectedObject);
}

void Renderer::UpdateGrid(){
  delete this->Grid;
  this->Grid = static_cast<RenderTexture*>(CreateGrid(*GC));
}


void Renderer::ResizeGrid(SDL_Point oldSize, SDL_Point newSize){
  double ratio[2];
  ratio[0] = (double)(newSize.x/oldSize.x);
  ratio[1] = (double)(newSize.y/oldSize.y);
  for(auto ro : ros){
    ro->Resize(ratio);
  }
}

void Renderer::ActivateLayer(Layer *layer){
  if(activeLayer){
    activeLayer->status |= LAYER_STATUS_MODIFIED;
    activeLayer->status &= ~LAYER_STATUS_ACTIVE;
  }
  layer->status |= LAYER_STATUS_ACTIVE;
  activeLayer = layer;
  ClearSelectedObject();
}

void Renderer::AddRoToActiveLayer(RenderObject *ro){
  if(activeLayer){
    ro->layer = activeLayer->id;
    activeLayer->ros.push_back(ro->id);
  }
}

void Renderer::GenLayers(){
  for(Layer* l : layers){
    if(! (l->status & LAYER_STATUS_INITIALLIZED)){
      //std::cout << "INITIALIZING LAYER" << std::endl;
      InitLayer(l);
    }
  }
}
void Renderer::InitLayer(Layer* l){
  int x, y;
  SDL_GetWindowSize(windowData.window, &x, &y);
  l->texture = SDL_CreateTexture(windowData.renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, x, y);
  l->status |= LAYER_STATUS_INITIALLIZED;
}
