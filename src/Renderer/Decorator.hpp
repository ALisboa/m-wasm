#ifndef DECORATOR_H
#define DECORATOR_H
#include "RenderObject.hpp"
#include "RenderLine.hpp"

class Decorator: public RenderTexture{
        public:
                Uint32 Color;
                RenderObject* base;
                Decorator();
                void Render(SDL_Renderer* renderer);
                MouseState GetCursor(SDL_Point pos);
                ~Decorator();
                void Generate(RenderObject* base);
                bool active;
                uint activeSize;
        private:
                void RenderT(SDL_Renderer* renderer);
                void RenderL(SDL_Renderer* renderer);
                MouseState GetCursorT(SDL_Point pos);
                MouseState GetCursorL(SDL_Point pos);
};


#endif
