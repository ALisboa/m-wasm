#include "RenderLine.hpp"
#include <stdexcept>
#include <cmath>
#include "../MathHelper.hpp"
#include <SDL2/SDL2_gfxPrimitives.h>

void RenderLine::Resize(double ratio[2]){
    Start = {(int)(std::round(Start.x * ratio[0])), (int)(std::round(Start.y*ratio[1]))};
    End = {(int)std::round(End.x* ratio[0]), (int)std::round(End.y*ratio[1])};
}


RenderLine::RenderLine(){
    Type = RENDER_TYPE_LINE;
    removed = false;
}

bool RenderLine::Inside(int x, int y){
    return Distance(Start.x, Start.y, End.x, End.y, x, y) < 5;
}

void RenderLine::Render(SDL_Renderer* renderer){
    SDL_SetRenderDrawColor(renderer, 0x00, 0xFF, 0x00, 0xFF);
    thickLineColor(renderer, Start.x, Start.y, End.x, End.y, width, color);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
}
