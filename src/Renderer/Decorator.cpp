#include "Decorator.hpp"
#include "../MathHelper.hpp"
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <SDL2/SDL2_gfxPrimitives.h>

#define a(A) (0xFF000000&A)>>24
#define r(A) (0x00FF0000&A)>>16
#define g(A) (0x0000FF00&A)>>8
#define b(A) (0x000000FF&A)
Decorator::Decorator(){
    this->activeSize = 8;
    this->Color = 0xFF00FF00;
}

Decorator::~Decorator(){
    
}

void Decorator::Render(SDL_Renderer* renderer){
    if(this->base){
        switch(this->base->Type){
            case RENDER_TYPE_TEXTURE:{
                RenderT(renderer);
                break;
            }
            case RENDER_TYPE_LINE:{
                RenderL(renderer);
                break;
            }
            default:{
                throw std::invalid_argument("Can't Render Object Type");
            }
        }
    }
}

void Decorator::RenderL(SDL_Renderer* renderer){
    SDL_Rect r;
    r.h = r.w = activeSize;
    RenderLine* l = (RenderLine*)base;
    r.x = l->Start.x - (activeSize/2);
    r.y = l->Start.y - (activeSize/2);
    SDL_SetRenderDrawColor(renderer, r(Color), g(Color), b(Color), a(Color));
    SDL_RenderDrawLine(renderer, l->Start.x, l->Start.y, l->End.x, l->End.y);
    SDL_RenderFillRect(renderer, &r);
    r.x = l->End.x - (activeSize/2);
    r.y = l->End.y - (activeSize/2);
    SDL_RenderFillRect(renderer, &r);
}

void Decorator::RenderT(SDL_Renderer* renderer){
    /* CREATE TEXTURE */
    SDL_Rect r;
    int a;
    RenderTexture* b = (RenderTexture*)this->base;
    auto texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_TARGET, Size.x, Size.y);
    SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
    SDL_SetRenderTarget(renderer, texture);

    SDL_SetRenderDrawColor(renderer, r(Color), g(Color), b(Color), a(Color));
    SDL_RenderDrawLine(renderer, 25, 25, 25+b->Size.x, 25);
    SDL_RenderDrawLine(renderer, 25, 25, 25, 25+b->Size.y);
    SDL_RenderDrawLine(renderer, 25+b->Size.x, 25+b->Size.y, 25+b->Size.x, 25);
    SDL_RenderDrawLine(renderer, 25+b->Size.x, 25+b->Size.y, 25, 25+b->Size.y);


    r.x = 25; r.y = 25; r.h = activeSize; r.w = activeSize;
    SDL_RenderFillRect(renderer, &r);
    r.y = 25+b->Size.y - activeSize;
    SDL_RenderFillRect(renderer, &r);
    r.x = 25+b->Size.x-activeSize; r.y = 25;
    SDL_RenderFillRect(renderer, &r);
    r.y = 25+b->Size.y-activeSize;
    SDL_RenderFillRect(renderer, &r);
    r.x = (Size.x-activeSize)/2; r.y = 0;
    SDL_RenderFillRect(renderer, &r);

    SDL_SetRenderTarget(renderer, NULL);
    /* COPY TEXTURE TO RENDERED SURFACE */
    SDL_Rect dst = { .x = Position.x, .y = Position.y, .w = Size.x, .h = Size.y };
    SDL_RenderCopyEx(renderer, texture, NULL, &dst, b->Angle, NULL, SDL_FLIP_NONE);
    /* Free Temp Texture*/
    SDL_DestroyTexture(texture);
}

void Decorator::Generate(RenderObject* base){
    this->base = base;
    RenderTexture* rt = (RenderTexture*)base;
    switch(base->Type){
        case RENDER_TYPE_TEXTURE:{
            this->Size = rt->Size;
            this->Position = rt->Position;
            this->Size.x += 50;
            this->Size.y += 50;
            this->Position.x -= 25;
            this->Position.y -= 25;
            break;
        }
        case RENDER_TYPE_LINE: {
            break;
        }
        defautl: {
            break;
        }
    }
}

MouseState Decorator::GetCursor(SDL_Point pos){
    switch(base->Type){
        case RENDER_TYPE_TEXTURE:{
            return GetCursorT(pos);
        }
        case RENDER_TYPE_LINE: {
            return GetCursorL(pos);
        }
        default: {
            return MOUSE_STATE_ARROW;
        }
    }
}

MouseState Decorator::GetCursorL(SDL_Point pos){
    RenderLine* l = (RenderLine*)base;
    if(ManhattanDistance(pos, l->Start) < activeSize || ManhattanDistance(pos, l->End) < activeSize ){
        return MOUSE_STATE_HAND;
    }
    return MOUSE_STATE_ARROW;
}
    
MouseState Decorator::GetCursorT(SDL_Point pos){
    /* Relative Points */
    RenderTexture* b = (RenderTexture*)base;
    int hs = activeSize/2;
    SDL_Point center = Position + Size/2;
    SDL_Point Hand = {Size.x/2, hs};
    Hand = Hand + Position;
    SDL_Point NW = {hs, hs};
    NW = b->Position + NW;
    SDL_Point SW = {hs, b->Size.y - hs};
    SW = SW + b->Position;
    SDL_Point SE = {b->Size.x - hs, b->Size.y - hs};
    SE = SE + b->Position;
    SDL_Point NE = {b->Size.x - hs, 0 + hs};
    NE = NE + b->Position;
    /* Translate Points */
    Hand = Hand - center;
    NE = NE - center;
    NW = NW - center;
    SE = SE - center;
    SW = SW - center;

    /* Rotate Point */    
    Hand = rotate(Hand, b->Angle);
    NE = rotate(NE, b->Angle);
    SE = rotate(SE, b->Angle);
    SW = rotate(SW, b->Angle);
    NW = rotate(NW, b->Angle);
    /* Translate Points */
    Hand = Hand + center;
    NE = NE + center;
    SE = SE + center;
    SW = SW + center;
    NW = NW + center;
    /* Check Distances */
    if(ManhattanDistance(pos, Hand) < activeSize){
        return MOUSE_STATE_HAND;
    }
    if(ManhattanDistance(pos, NE) < activeSize){
        return MOUSE_STATE_NE;
    }
    if(ManhattanDistance(pos, SE) < activeSize){
        return MOUSE_STATE_SE;
    }
    if(ManhattanDistance(pos, SW) < activeSize){
        return MOUSE_STATE_SW;
    }
    if(ManhattanDistance(pos, NW) < activeSize){
        return MOUSE_STATE_NW;
    }
    return MOUSE_STATE_ARROW;

}
