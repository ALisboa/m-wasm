#include "RenderObject.hpp"
#include <stdexcept>
#include <cmath>
#include "../MathHelper.hpp"
#include <assert.h>


void RenderObject::Render(SDL_Renderer* renderer){
  throw std::logic_error("ABSTRACT CLASS METHOD");
}

bool RenderObject::Inside(int x, int y){
  return false;
}


bool RenderTexture::Inside(int x, int y){
  return ( !(Position.x > x || x > Position.x + Size.x ) && 
           !(Position.y > y || y > Position.y + Size.y ) );
}


RenderTexture::~RenderTexture(){
  //SDL_DestroyTexture(_Texture->txt_ptr);
}

void RenderTexture::Render(SDL_Renderer* renderer) {
  SDL_Rect dst = { .x = Position.x, .y = Position.y, .w = Size.x, .h = Size.y };
  if(TM->TextureMap.count(TextureID)){
    SDL_RenderCopyEx(renderer, TM->GetTexture(TextureID)->txt_ptr, NULL, &dst, Angle, NULL, SDL_FLIP_NONE);
  }
}


void RenderObject::Resize(double ratio[2]){

}

void RenderTexture::Resize(double ratio[2]){
  Position = {(int)std::round(Position.x*ratio[0]), (int)std::round(Position.y*ratio[1])};
  Size = {(int)std::round(Size.x*ratio[0]), (int)std::round(Size.y*ratio[1])};
}

RenderTexture::RenderTexture(){
  removed = false;
  Angle = 0;
  Position = {0, 0};
  Type = RENDER_TYPE_TEXTURE;
}

SDL_Point RenderTexture::NW(){
    auto nw = rotate(-(Size/2), Angle);
    return nw + Position + Size/2;
}

SDL_Point RenderTexture::NE(){
  auto ne = rotate({Size.x/2, -Size.y/2}, Angle);
  return ne + Position + Size/2;
}

SDL_Point RenderTexture::SW(){
  auto sw = rotate( {-Size.x/2, Size.y/2}, Angle);
  return sw + Position + Size/2;
}

SDL_Point RenderTexture::SE(){
  auto se = rotate(Size/2, Angle);
  return se + Position + Size/2;
}

SDL_Point RenderTexture::Center(){
  return Position + Size/2;
}
