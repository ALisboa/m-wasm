#include "TextureManager.hpp"
#include <iostream>

TextureManager::TextureManager(){
    nid = 1;
}

TextureManager::~TextureManager(){

}

Texture* TextureManager::NewEmptyTexture(){
    Texture* txt = new Texture();
    txt->id = nid++;
    TextureMap[txt->id] = txt;
    return txt;
}

Texture* TextureManager::AddTexture(SDL_Texture* texture, std::string filename){
    Texture* txt = NewEmptyTexture();
    txt->filename = filename;
    txt->txt_ptr = texture;
    return txt;
}

Texture* TextureManager::AddTextureWithID(SDL_Texture* texture, std::string filename, int id){
    if(TextureMap.count(id)){
        std::cerr << "ERROR TEXTURE ID: " << id << " EXIST" << std::endl;
        return NULL;
    }
    if(id >= nid ) {
        nid = id + 1;
    }
    //TODO: DEAL WITH ERRORS
    Texture* txt = new Texture();
    txt->id = id;
    txt->filename = filename;
    txt->txt_ptr = texture;
    TextureMap[id] = txt;
    return txt;
}

Uint32 TextureManager::SetTexture(Uint32 i, SDL_Texture* texture){
    return 0;
}

Texture* TextureManager::GetTexture(Uint32 i){
	if(TextureMap.count(i)){
		return TextureMap[i];
	}
	return NULL;
}

