#ifndef RENDEROBJECT_H
#define RENDEROBJECT_H
#include "types.hpp"
#include <SDL2/SDL.h>
#include "TextureManager.hpp"

extern TextureManager* TM;


class RenderObject {
    public:
        int id;
        int Z;
        int layer;
        bool removed;
        RenderType Type;
        virtual void Render(SDL_Renderer* renderer);
        virtual bool Inside(int x, int y);
        virtual void Resize(double ratio[2]);
};

class RenderTexture : public RenderObject {
    public:
        SDL_Point Size;
        SDL_Point Position;
        double Angle;
        int TextureID;
        void Render(SDL_Renderer* renderer);
        void Resize(double ratio[2]);
        bool Inside(int x, int y);
        RenderTexture();
        ~RenderTexture();
        SDL_Point NW();
        SDL_Point NE();
        SDL_Point SW();
        SDL_Point SE();
        SDL_Point Center();
};

#endif
