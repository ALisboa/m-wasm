#ifndef GRID_H
#define GRID_H 
#include <SDL2/SDL.h>

enum GridType {
  SQUARED, HEXAGON
};

enum AnchorPoints {
  CORNER = 1,
  MID_SECTIONS = 1<<1,
  CENTER = 1 << 2
};

struct GridConfig {
  SDL_Point TileSize;
  SDL_Point Size;
  int Anchors;
  Uint32 BackgroundColor;
  Uint8 width;
  Uint32 GridColor;
};

SDL_Point CloseCenterAnchor(SDL_Point position, SDL_Point size);
SDL_Point CloseMidSectionAnchor(SDL_Point position, SDL_Point size);
SDL_Point CloseCornerAnchor(SDL_Point position, SDL_Point size);
SDL_Point AproxToAnchor(SDL_Point pos, GridConfig config);
SDL_Point TopLeftCorner(SDL_Point pos, SDL_Point size);
#endif
