#ifdef __EMSCRIPTEN__
#include "ExternalLibrary.hpp"
#include "Controller.hpp"
#include <iostream>

extern "C" {

    void EMSCRIPTEN_KEEPALIVE ChangeLineConfig(int color, int width){
        controller.ChangeLineConfig((Uint32) color, (Uint16) width);
    }

    void EMSCRIPTEN_KEEPALIVE ResizeWindow(int w, int h){
        re.ResizeWindow({w, h});
    }

    void EMSCRIPTEN_KEEPALIVE SetGridWidth(int size){
        controller._GridConfig.width = size;
        re.UpdateGrid();
    }

    void EMSCRIPTEN_KEEPALIVE ToggleVisionMode(){
        controller.VisibilityMode();
    }

    void EMSCRIPTEN_KEEPALIVE SetTileSize(int x, int y){
        re.ResizeGrid(controller._GridConfig.TileSize, {x, y});
        controller._GridConfig.TileSize = {x, y};
        re.ResizeWindow({controller._GridConfig.Size.x*x, controller._GridConfig.Size.y*y});
        re.UpdateGrid();
    }

    void EMSCRIPTEN_KEEPALIVE SetGridSize(int x, int y){
        controller._GridConfig.Size = {x, y};
        re.ResizeWindow({controller._GridConfig.TileSize.x*x, controller._GridConfig.TileSize.y*y});
        re.UpdateGrid();
    }

    void EMSCRIPTEN_KEEPALIVE SetGridColor(Uint32 color){
        std::cout << std::hex << color << std::endl;
        controller._GridConfig.GridColor = color;
        re.UpdateGrid();
    }

    void EMSCRIPTEN_KEEPALIVE SetGridBackgroundColor(Uint32 color){
        std::cout << color << std::endl;
        controller._GridConfig.BackgroundColor = color;
        re.UpdateGrid();
    }

    int EMSCRIPTEN_KEEPALIVE AddImage(const char* filename){
        return controller.NewImage(filename);
    }

    int EMSCRIPTEN_KEEPALIVE InsertImage(const char* filename, int id) {
        return controller.InsertImage(filename, id);
    }

    int EMSCRIPTEN_KEEPALIVE AddRo(int TextureID){
        return controller.AddRo(TextureID);
    }

    int EMSCRIPTEN_KEEPALIVE AddRoIn(int TextureID, int x, int y) {
        return controller.AddRoIn(TextureID, x, y);
    }
    int EMSCRIPTEN_KEEPALIVE SetRO(int id, int index, int layer, int idTexture){
        return controller.SetRO(id, index, layer, idTexture);
    }

    int EMSCRIPTEN_KEEPALIVE ResizeRO(int roid, int sizex, int sizey){
        return controller.ResizeRO(roid, {sizex, sizey});
    }

    int EMSCRIPTEN_KEEPALIVE PosRO(int roid, int posx, int posy){
        return controller.PosRO(roid, {posx, posy});
    }

    const char * EMSCRIPTEN_KEEPALIVE GetError(){
        return "error";
    }

    void EMSCRIPTEN_KEEPALIVE SetRestartParser(void * funcPointer){
        RP = reinterpret_cast<RestartParser>(funcPointer);

        //RP();
        //std::cout << 1 << std::endl;
        //std::cout << RP << std::endl;
    }

    void EMSCRIPTEN_KEEPALIVE Q(){
        done = true;
        QUIT();
    }
/*
 * 1 = Drawing
 * 2 = Selection
 */
    void EMSCRIPTEN_KEEPALIVE SetState(int input ){
        input -= 1;
        switch(input){
            case SYSTEM_STATE_DRAWING:
                controller.ChangeState(SYSTEM_STATE_DRAWING);
                break;
            case SYSTEM_STATE_SELECTION:
                controller.ChangeState(SYSTEM_STATE_SELECTION);
                break;
            default:
                break;
        }
    }

    int EMSCRIPTEN_KEEPALIVE SwitchLayerByName(std::string name){
        return controller.SwitchLayer(name);
    }

    int EMSCRIPTEN_KEEPALIVE SwitchLayerByID(int id){
        return controller.SwitchLayer(id);
    }

    int EMSCRIPTEN_KEEPALIVE ToggleSpectator(){
        controller.spectator = !controller.spectator;
        return controller.spectator;
    }

    int EMSCRIPTEN_KEEPALIVE SerializedUpdate(char * string){
        std::string update(string);
        //std::cerr << update << std::endl;
        return controller.SerializedUpdate(update);
    }

    void EMSCRIPTEN_KEEPALIVE print(char * string){
        std::cerr << "PRINTING: " << string << std::endl;
    }

    void EMSCRIPTEN_KEEPALIVE GetSelectedObject(){
        //RestartParser();
        //std::cout << PARSER_NGMSG << std::endl;
        //auto obj = controller.getSelectedObject();
    }

    void EMSCRIPTEN_KEEPALIVE PrintCurrentLayers(){
        controller.PrintLayerState();
    }

    void EMSCRIPTEN_KEEPALIVE GenGM(){
        controller.GenDemoGM();
    }

    int EMSCRIPTEN_KEEPALIVE SwitchUser(int id){
       return controller.SwitchUser(id);
    }

    int EMSCRIPTEN_KEEPALIVE ModifyUser(int id, char* string){
       return controller.ModifyUser(id, string);
    }

    int EMSCRIPTEN_KEEPALIVE MakeToken(int ro_id){
        return controller.MakeToken(ro_id);
    }

    void EMSCRIPTEN_KEEPALIVE RemoveRo(int id){
         controller.RemoveRo(id);
    }

    void EMSCRIPTEN_KEEPALIVE ChangeRoLayer(int ro_id, int layer_id){
         controller.ChangeToLayer(ro_id, layer_id);
    }

    void EMSCRIPTEN_KEEPALIVE RemoveSelected() {
        controller.RemoveSelected();
    }

    void EMSCRIPTEN_KEEPALIVE CopySelected() {
        controller.CopySelected();
    }

    void EMSCRIPTEN_KEEPALIVE PasteFromClipboard(){
        controller.PasteFromClipBoard();
    }

}

#endif
