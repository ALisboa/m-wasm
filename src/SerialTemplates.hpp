#ifndef __SERIALTEMPLATES_H_
#define __SERIALTEMPLATES_H_
#include "Renderer/RenderObject.hpp"
#include "Renderer/RenderLine.hpp"
#include "Renderer/Layer.hpp"
#include <SDL2/SDL.h>
#include <string>
#include "Players.hpp"
#include "Token.hpp"
#include "MessageHelper.hpp"
#include <cereal/types/common.hpp>
#include <cereal/archives/xml.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/types/vector.hpp>
#define CEREAL_INPUT_ARCHIVE cereal::XMLInputArchive
#define CEREAL_OUTPUT_ARCHIVE cereal::XMLOutputArchive

enum SerialType{
SERIAL_TYPE_RENDER_LINE, /* RenderLine	*/
SERIAL_TYPE_RENDER_TEXTURE, /* RenderTexture */
SERIAL_TYPE_LAYER,  /* Layer */
SERIAL_TYPE_PLAYER, /* Player */
SERIAL_TYPE_TOKEN,
};

struct SerializedObj{
    SerialType type;
    std::string archive;
};

template<class Archive>
void serialize(Archive &archive, SerializedObj& obj){
    archive(obj.type, obj.archive);
}

template<class Archive>
void serialize(Archive& archive, RenderLine& rl){
    archive(
        CEREAL_NVP(rl.id),
        CEREAL_NVP(rl.Type),
        CEREAL_NVP(rl.Z),
        CEREAL_NVP(rl.layer),
        CEREAL_NVP(rl.removed),
        CEREAL_NVP(rl.Start),
        CEREAL_NVP(rl.End),
        CEREAL_NVP(rl.Z),
        CEREAL_NVP(rl.width),
        CEREAL_NVP(rl.color)
    );
}

template<class Archive>
void serialize(Archive &archive, RenderTexture &rl){
    archive(rl.id, rl.Type, rl.Z, rl.layer, rl.removed, rl.Position, rl.Size, rl.Angle, rl.Z, rl.TextureID);
}

template<class Archive>
void serialize(Archive &archive, SDL_Point &rl){
    archive(rl.x, rl.y);
}
template<class Archive>
void serialize(Archive &archive, Layer &l){
    archive(CEREAL_NVP(l.id),
            CEREAL_NVP(l.layer_type),
            CEREAL_NVP(l.ros),
            CEREAL_NVP(l.status), CEREAL_NVP(l.alpha));
}
template<class Archive>
void serialize(Archive &archive, VisibilityLayer &l){
    archive(CEREAL_NVP(l.id),
            CEREAL_NVP(l.layer_type),
            CEREAL_NVP(l.ros),
            CEREAL_NVP(l.status),
            CEREAL_NVP(l.alpha),
            CEREAL_NVP(l.visionPoints)
    );
}
struct LayerEditorHelper{
    std::vector<Layer> Texture_layers;
    std::vector<VisibilityLayer> Vision_layers;
    template <class Archive>
    void serialize(Archive &archive){
        archive(CEREAL_NVP(Texture_layers), CEREAL_NVP(Vision_layers));
    }
};
template<class Archive>
void serialize(Archive &archive, Player &p){
    archive(
        CEREAL_NVP(p.id),
        CEREAL_NVP(p.name),
        CEREAL_NVP(p.flags)
    );
}
template<class Archive>
void serialize(Archive &archive, Token &t){
    archive(
        CEREAL_NVP(t.id),
        CEREAL_NVP(t.user_id),
        CEREAL_NVP(t.ro_id)
    );
}
template<class Archive>
void serialize(Archive &archive, RenderObject &obj){
    archive(
        CEREAL_NVP(obj.id),
        CEREAL_NVP(obj.Z),
        CEREAL_NVP(obj.layer),
        CEREAL_NVP(obj.removed),
        CEREAL_NVP(obj.Type)
    );
}
template<class Archive>
void serialize(Archive &archive, RightClickStatus &status){
    archive(
        cereal::make_nvp("roSelected", status.roSelected),
        cereal::make_nvp("layerId", status.layerId),
        cereal::make_nvp("systemState", status.systemState),
        cereal::make_nvp("ro", (*status.ro))
    );
}


#endif // __SERIALTEMPLATES_H_
