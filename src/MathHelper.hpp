#ifndef MATHHELPER_H
#define MATHHELPER_H 
#include <SDL2/SDL.h>
#include <vector>
#include <iostream>
#define RAD2DEG 57.2958


struct IT {
	double x,y,t;
	double angle;
};

struct Point{
	double x;
	double y;
};

struct Ray{
	Point a;
	Point b;
};

struct PolyPoint{
	Sint16 x, y;
	bool operator()(PolyPoint s){
		return (x < s.x) || ((x == s.x) && (y < s.y));
	}
};

extern SDL_BlendMode BLENDMODE_AMINUS;

SDL_Point operator +(SDL_Point a, SDL_Point b);
SDL_Point operator -(SDL_Point);
SDL_Point operator -(SDL_Point a, SDL_Point b);
SDL_Point operator /(SDL_Point a, int b);
bool operator ==(const SDL_Point &a, const SDL_Point &b);
bool operator !=(const SDL_Point &a, const SDL_Point &b);
bool operator <(const SDL_Point &a, const SDL_Point &b);
int ManhattanDistance(SDL_Point a, SDL_Point b);
SDL_Point rotate(SDL_Point a, double angle);

std::ostream& operator << (std::ostream& o, const IT t);


bool itc(IT a, IT b);

bool cmp(PolyPoint a, PolyPoint b);

bool operator <(const Point a, const Point b);


std::vector<PolyPoint> GenSightPoly(Point point, std::vector<Ray> segments);

IT Intersect(Ray r, Ray Segment);

int Distance(Ray a, Point b);
int Distance(int x1, int y1, int x2, int y2, int x0, int y0);
int Distance(Ray a, int x0, int y0);
int Distance(int x1, int y1, int x2, int y2, Point b);

#endif
