#include "MathHelper.hpp"
#include <cmath>
#include <set>
#include <algorithm>

SDL_Point operator -(SDL_Point a, SDL_Point b){
  return a+(-b);
}

SDL_Point operator +(SDL_Point a, SDL_Point b){
  return {a.x + b.x, a.y + b.y};
}

SDL_Point operator -(SDL_Point a){
  return {-a.x, -a.y};
}

SDL_Point operator /(SDL_Point a, int b){
  return {a.x/b, a.y/b};
}

int ManhattanDistance(SDL_Point a, SDL_Point b){
  return abs(a.x - b.x) + abs(a.y - b.y);
}

SDL_Point rotate(SDL_Point a, double degree){
  double rad = (double)(degree/RAD2DEG);
  double s = std::sin(rad);
  double c = std::cos(rad);
  SDL_Point n = {
    (int)std::round(a.x * c - a.y * s),
    (int)std::round(a.x * s + a.y * c)
  };
  return n;
}

bool operator< (const SDL_Point& a, const SDL_Point& b){
	return a.x == b.x && a.y == b.y;
}

bool itc(IT a, IT b){
	return a.angle < b.angle;
}

bool cmp(PolyPoint a, PolyPoint b){
	return a.x == b.x && a.y == b.y;
}

bool uniquePoint(Point a, Point b){
	return a.x == b.x && a.y == b.y;
}

IT Intersect(Ray r, Ray segment){
	double r_px = r.a.x;
	double r_py = r.a.y;
	double r_dx = r.b.x-r.a.x;
	double r_dy = r.b.y-r.a.y;

	double s_px = segment.a.x;
	double s_py = segment.a.y;
	double s_dx = segment.b.x - s_px;
	double s_dy = segment.b.y - s_py;

	double r_mag = std::sqrt(r_dx*r_dx + r_dy*r_dy);
	double s_mag = std::sqrt(s_dx*s_dx + s_dy*s_dy);

	if(r_dx/r_mag==s_dx/s_mag && r_dy/r_mag==s_dy/s_mag){
		return {-1, -1, -1, -1};
	}

	double T2 = (r_dx*(s_py-r_py) + r_dy*(r_px-s_px))/(s_dx*r_dy - s_dy*r_dx);
	double T1 = (s_px+s_dx*T2-r_px)/r_dx;

	if(T1 < 0){
		return {-1, -1, -1, -1};
	}
	if(T2 < 0 || T2 > 1){
		return {-1, -1, -1, -1};
	}

	return {
		r_px + r_dx*T1,
		r_py + r_dy*T1,
		T1, -1
	};
}

bool uniquedouble(double a, double b){
	return a == b;
}

std::vector<PolyPoint> GenSightPoly(Point point, std::vector<Ray> segments){
	std::vector<PolyPoint> polygon;
	std::vector<IT> intersects;
	std::vector<Point> unique_pts;
	for(auto r: segments){
		unique_pts.push_back(r.a);
		unique_pts.push_back(r.b);
	}
	auto iterador = std::unique(unique_pts.begin(), unique_pts.end(), uniquePoint);
	unique_pts.resize(std::distance(unique_pts.begin(), iterador));
	std::vector<double> unique_angles;
	for(Point a : unique_pts){
		float angle = std::atan2(a.y - point.y, a.x - point.x);
		if(angle < 0){
			//angle = 360/RAD2DEG + angle;
		}
		unique_angles.push_back(angle-0.0001f);
		unique_angles.push_back(angle);
		unique_angles.push_back(angle+0.0001f);
	}
	std::unique(unique_angles.begin(), unique_angles.end());
	Ray r;
	float dx, dy;
	for(float a: unique_angles){
			IT closest = {-1, -1, -1};
			dx = std::cos(a);
			dy = std::sin(a);

			r = {
				{(double)point.x, (double)point.y},
				{point.x + dx, point.y + dy}
			};

			for(auto seg: segments){
				IT result = Intersect(r, seg);
				if(result.t < 0){
					continue;
				}
				if(closest.t < 0 || result.t < closest.t){
					closest = result;
				}
			}
	
			if(closest.t < 0){
				continue;
			}
			closest.angle = a;
			intersects.push_back(closest);
	}

	std::sort(intersects.begin(), intersects.end(), itc);
	
	for(IT a : intersects){
		Sint16 x = (Sint16)(a.x);
		Sint16 y = (Sint16)(a.y);
		polygon.push_back({x,y});
	}

	return polygon;
}

bool operator <(const Point a, const Point b)
{
	return !(a.x == b.x && a.y == b.y);
}

std::ostream& operator << (std::ostream& o, const IT t){
	o << "X: " << t.x << "\tY: " << t.y << "\tT: " << t.t << "\tAngle: " << t.angle;
 return o;	
}

int Distance(Ray a, Point b){
	return std::abs((a.b.y - a.a.y)*b.x - (a.b.x - a.a.x)*b.y + a.b.x*a.a.y - a.b.y*a.a.x) / std::sqrt( std::pow(a.b.y - a.a.y, 2) + std::pow(a.b.x - a.a.x, 2));
}

int Distance(int x1, int y1, int x2, int y2, int x0, int y0){
	return Distance({{(double)x1, (double)y1}, {(double)x2,(double) y2}}, {(double)x0, (double)y0});
}

int Distance(Ray a, int x0, int y0){
	return Distance(a, {(double)x0, (double)y0});
}

int Distance(int x1, int y1, int x2, int y2, Point b){
	return Distance({{(double)x1, (double)y1},{(double)x2, (double)y2}}, b);
}

bool operator ==(const SDL_Point &a, const SDL_Point &b){
	return a.x == b.x && a.y == b.y;
}

bool operator !=(const SDL_Point &a, const SDL_Point &b){
	return !(a == b);
}

