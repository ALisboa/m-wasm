#ifndef MESSAGE_HELPER_H
#define MESSAGE_HELPER_H
#include <string>
#include <sstream>
#include "Renderer/RenderObject.hpp"

typedef void (*RestartParser)(void);
extern RestartParser RP;

// void SendAction(std::string s);
void SendMessage(std::string message);
void SendError(std::string message);
void SendUpdate(std::string message);
void SendRightClick(std::string message);
void _PrintMessage(std::string premessage, std::string message);

struct RightClickStatus {
    bool roSelected;
    int layerId;
    int systemState;
    RenderObject* ro;
};

#endif
