#include <SDL2/SDL.h>
#include <Renderer/RenderObject.hpp>

struct MoveEvent{
	int ROid;
	int rotate;
	SDL_Point newPos;
	SDL_Point newSize;
};
