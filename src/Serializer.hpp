#ifndef SERIALIZER_H
#define SERIALIZER_H
#include "SerialTemplates.hpp"

void UpdateObject(RenderObject* ob);
void UpdateObject(RenderLine* ob);
void UpdateObject(RenderTexture* ob);
void UpdateObject(Layer* ob);
void UpdateObject(Player* ob);
void UpdateObject(Token* ob);
void UpdateObject(SerialType type, std::string archive);
std::string Serialize(RenderLine* ob);
std::string Serialize(RenderTexture* ob);
std::string Serialize(Layer* ob);
std::string Serialize(VisibilityLayer* ob);
std::string Serialize(Token* ob);
std::string Serialize(Player* ob);
std::string Serialize(SerializedObj so);
std::string _CleanArchive(std::string archive);
template<class T>
T Deserialize(std::string archive);

template<class T>
std::string MakeJson(T object);

#endif
