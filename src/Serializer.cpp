#include "Serializer.hpp"
#include "MessageHelper.hpp"
#include <string>
#include <sstream>
#include <algorithm>

void UpdateObject(RenderObject* ob){
    switch(ob->Type){
        case RENDER_TYPE_LINE: {
            UpdateObject(dynamic_cast<RenderLine*>(ob));
            break;
        }
        case RENDER_TYPE_TEXTURE: {
            UpdateObject(dynamic_cast<RenderTexture*>(ob));
            break;
        }
        default: {
            // TODO THROW EXCEPTION
            break;
        }
    }
}

void UpdateObject(RenderLine* ob){
    UpdateObject(SERIAL_TYPE_RENDER_LINE, Serialize(ob));
}

void UpdateObject(RenderTexture* ob){
    UpdateObject(SERIAL_TYPE_RENDER_TEXTURE, Serialize(ob));
}

void UpdateObject(Layer* ob){
    UpdateObject(SERIAL_TYPE_LAYER, Serialize(ob));
}

void UpdateObject(Player* ob){
    UpdateObject(SERIAL_TYPE_PLAYER, Serialize(ob));
}

void UpdateObject(Token* ob){
    UpdateObject(SERIAL_TYPE_TOKEN, Serialize(ob));
}

void UpdateObject(SerialType type, std::string archive){
    SerializedObj so = {type, archive};
    SendUpdate(Serialize(so));
}

std::string Serialize(RenderLine* ob){
    std::stringstream ss;
    {
        CEREAL_OUTPUT_ARCHIVE archive(ss);
        archive(*ob);
    }
    std::string archive = ss.str();
    return _CleanArchive(archive);
}

std::string Serialize(RenderTexture* ob){
    std::stringstream ss;
    {
        CEREAL_OUTPUT_ARCHIVE archive(ss);
        archive(*ob);
    }
    std::string archive = ss.str();
    return _CleanArchive(archive);
}

std::string Serialize(Layer* ob){
    std::stringstream ss;
    {
        CEREAL_OUTPUT_ARCHIVE archive(ss);
        archive(*ob);
    }
    std::string archive = ss.str();
    return _CleanArchive(archive);
}

std::string Serialize(VisibilityLayer* ob){
    std::stringstream ss;
    {
        CEREAL_OUTPUT_ARCHIVE archive(ss);
        archive(*ob);
    }
    std::string archive = ss.str();
    return _CleanArchive(archive);
}

std::string Serialize(Player* ob){
    std::stringstream ss;
    {
        CEREAL_OUTPUT_ARCHIVE archive(ss);
        archive(*ob);
    }
    std::string archive = ss.str();
    return _CleanArchive(archive);
}

std::string Serialize(Token* ob){
    std::stringstream ss;
    {
        CEREAL_OUTPUT_ARCHIVE archive(ss);
        archive(*ob);
    }
    std::string archive = ss.str();
    return _CleanArchive(archive);
}

std::string Serialize(SerializedObj so){
    std::stringstream ss;
    std::string archiveString;
    {
        CEREAL_OUTPUT_ARCHIVE archive(ss);
        archive(so);
    }
    archiveString = ss.str();
    return _CleanArchive(archiveString);
}

std::string _CleanArchive(std::string archive){
    archive.erase(
        std::remove(
            archive.begin(),
            archive.end(),
            '\n'
        ),
        archive.end()
    );
    archive.erase(
        std::remove(
            archive.begin(),
            archive.end(),
            '\t'
        ),
        archive.end()
    );
    return archive;
}

template<typename T>
T Deserialize(std::string archive){
    T object;
    {
      std::stringstream ss(archive);
      CEREAL_INPUT_ARCHIVE cerealArchive(ss);
      cerealArchive(object);
    }
    return object;
}

template RenderLine Deserialize<RenderLine>(std::string);
template SerializedObj Deserialize<SerializedObj>(std::string);
template RenderTexture Deserialize<RenderTexture>(std::string);
template Player Deserialize<Player>(std::string);
template Token Deserialize<Token>(std::string);
template Layer Deserialize<Layer>(std::string);
template VisibilityLayer Deserialize<VisibilityLayer>(std::string);


template<typename T>
std::string MakeJson(T object){
    std::stringstream ss;
    std::string archiveString;
    {
        cereal::JSONOutputArchive archive(ss);
        archive(object);
    }
    archiveString = ss.str();
    return _CleanArchive(archiveString);
}

template std::string MakeJson<RightClickStatus>(RightClickStatus object);
template std::string MakeJson<RenderObject>(RenderObject object);
template std::string MakeJson<RenderTexture>(RenderTexture object);
template std::string MakeJson<SDL_Point>(SDL_Point object);
template std::string MakeJson<VisibilityLayer>(VisibilityLayer object);
template std::string MakeJson<Token>(Token object);
template std::string MakeJson<std::vector<Token>>(std::vector<Token> object);
template std::string MakeJson<RenderLine>(RenderLine object);
