#ifndef __PLAYERS_H_
#define __PLAYERS_H_
#include <string>

enum PlayerFlags{
PLAYER_FLAG_DM, PLAYER_FLAG_PLAYER, PLAYER_FLAG_NO_FLAG
};

class Player{
    private:
    public:
        unsigned int flags;
        std::string name;
        unsigned int id;
        Player();
        ~Player();
};

#endif // __PLAYERS_H_
