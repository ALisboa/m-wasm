# V20T-Motor Grafico

## Table of Contents

1.  [Compilación](#org1eafb6e)
2.  [Pruebas Unitarias](#org7dcf56c)

Este es el código para el Motor Grafico del Proyecto de Titulación de Alan Lisboa para la Carrera de Ingenieria civil en Computación de la Universidad de Talca.

## Compilación

Para compilar este proyecto es necesario utilizar la cadena de herramientas de Emscripten. Una ves el SDK ha sido cargado en el ambiente solo es necesario llamar a Emmake.

    emmake make


<a id="org7dcf56c"></a>

## Pruebas Unitarias

Para la tarea de pruebas unitarias es necesario compilar el proyecto en su modo binario. Para esto las siguientes herramientas son necesarias:

-   SDL2
-   SDL2<sub>image</sub>
-   SDL2<sub>gfx</sub>
-   SDL2-ttf

Luego, la compilación en modo de pruebas es mediante:

    make test

Ejecutar pruebas:

    ./build/grid.js

